HIPPO: the Hierarchical Inference of Posterior Precisions in OSL
---
Giles Colclough, 2016




A Matlab package for multiple-subject brain connectivity inference with fMRI and 
MEG. The code implements the hierarchical sparse Bayesian covariance model set 
out in 

Colclough, G. L. "Methods for modelling human functional brain connectivity with 
MEG and fMRI," DPhil thesis, University of Oxford, Oxford, UK, 2016.



To use the code, make sure this folder is on your Matlab path.

You'll also need:

  - The stats toolbox
  - Tom Minka's lightspeed toolbox (https://tminka.github.io/software/lightspeed/index.html)
  - The MEG-ROI-nets package (http://ohba-analysis.github.io)
  - The bluewhitered colour map (https://uk.mathworks.com/matlabcentral/fileexchange/4058-bluewhitered)
  - The hline/vline functions (https://uk.mathworks.com/matlabcentral/fileexchange/1039-hline-and-vline)
  - Copula (https://github.com/stochasticresearch/copula)






