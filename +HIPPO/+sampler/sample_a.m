function a = sample_a(BetaPrior, nEdges, nEdgesIn)
%SAMPLE_A draw hyper parameter for edge inclusion prior
% 
% Z_ij ~ Bernoulli(a)
%    a ~ Beta(a_pi, b_pi)
%
% a = sample_a(BETAPRIOR, NEDGES, NEDGESIN)
%
% a is the global probability of an edge

global SAMPLING_STAGE

% 1. If Prior.Beta.a is specified, then don't sample this parameter
if isfield(BetaPrior, 'a') && ~isempty(BetaPrior.a),
    a = BetaPrior.a;
% 2. if we're starting sampling, we don't update Z, so set a = 0.5.
elseif 1 == SAMPLING_STAGE ,
	a = 1;
% 3. Gibbs draws of a.
else
    a_post = BetaPrior.a_pi + nEdgesIn;
    b_post = BetaPrior.b_pi + nEdges - nEdgesIn;
    a      = randbeta(a_post, b_post);
end%if
end%sample_a