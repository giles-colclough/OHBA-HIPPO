function lambda = sample_lambda(LambdaPrior, p, sumDiagOmegaS)
%SAMPLE_LAMBDA draw diagonal regularisation terms
%
% lambda = sample_lambda(LAMBRAPRIOR, NMODES, SUM_DIAG_OMEGA_S)
%
% Draws regularisation terms for the exponential distributions 
% on the diagonal elements of the precision matrices from a 
% gamma hyperprior

nSubjects = length(sumDiagOmegaS);
if ~isfield(LambdaPrior, 'lambda') || isempty(LambdaPrior.lambda),
    % Gibbs draw of lambda
    shape_post = repmat(LambdaPrior.shape + p, 1, nSubjects);
    rate_post  = LambdaPrior.rate + 0.5 * sumDiagOmegaS;

    lambda = HIPPO.utils.stats.randgamma(shape_post, 1.0./rate_post);

else
    % use provided lambda: corresponds to delta-fn prior
    lambda = repmat(LambdaPrior.lambda, 1, nSubjects);
end%if
end%sample_lambda