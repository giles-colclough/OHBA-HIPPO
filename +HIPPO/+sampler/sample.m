function [Omega_s_save,  ...
          Sigma_s_save,  ...
          mu_save,       ...
          Z_save,        ...
          p_save,        ...
          chi_sq_save,   ...
          sigma_sq_save, ...
          lambda_save,   ...
          a_save,        ...
          accept_save,   ...
          lp_save,       ...
          acceptSigma_save] = sample(S, N, nWarmup, nSamples, ...
                                     Prior, saveWarmup, thin, ...
									 saveFileName, rndSeed)
									   
%SAMPLE an inference chain for HIPPO: the hierarchichal inference of posterior precisions
%
% [] = SAMPLE(S, N, NWARMUP, NSAMPLES, PRIOR, SAVEWARMUP, THIN, SAVEFILENAME, RNDSEED)
%   runs a chain of samples for the HIPPO model, saving the results into SAVEFILENAME. 
%   
%   HIPPO is a hierarchical Bayesian model for sparse covariance matrices that have an 
%   optional shared sparsity structure, and which are drawn from a common population. 
% 
%   The model is run on the set of sum-of-squares matrices S (nNodes x nNodes x nSubjects)
%   collected from each of the linked observations (e.g. subjects), and the number of 
%   time samples collected for each subject is held in N. The parameters in the PRIOR 
%   structure (described below) determine the behaviour of the model. 
% 
%   The sampler draws NWARMUP + NSAMPLES samples in total, of which NWARMUP are used to 
%   optimise the sampling algorithm. These warmup samples will be discarded if SAVEWARMUP 
%   is false, and only every THIN samples are saved. Samples are saved into SAVEFILENAME
%   if it is provided, else returned. 
% 
%   The sampler is initialised using RNDSEED, which will be generated from the current 
%   time if not supplied. 
% 
% [Omega_s, Sigma_s, mu, Z, p, chi_sq, sigma_sq, lambda, a, Z_accept, lp, sigma_accept] = SAMPLE(...)
% 
%   returns the sampled variables if a SAVEFILENAME is not provided. 
% 
%    -- key outputs
%    Omega_s - precision matrices for each subject (nEdges x nSubjects x nSamples)
%    Sigma_s - covariance matrices for each subject (nEdges x nSubjects x nSamples)
%    Z       - set of edges included in the model (nEdges x nSamples)
% 
%    -- parameters within the hierarchy
%    mu       - mean connection strengths on each edge
%    chi_sq   - variance of the normal distribution on mu
%    sigma_sq - variance of the subject edge strengths about mu
%    lambda   - regularisation on the diagonal elements of Omega_s
%    a        - probability of an edge (controls the sparsity of the model)
% 
%    -- diagonostics
%    Z_accept     - number of edges changed, for each sample, in each column (nNodes x nSamples)
%    sigma_accept - acceptance of the MH sampler on sigma_sq
%    lp           - the log probability of the model (only returned if DEBUG mode is on, as involves lots of extra computation)
% 
% 
%  Prior
%  -----
%    Prior.Beta   : Beta distribution on top-level sparsity parameter a
%                   a    - Just set a to a value, and don't use the hierarchical prior here
%                   a_pi - If a is not set, then a ~ beta(a_pi, b_pi) [6,6]
%                   b_pi
% 
%    Prior.Edges  : A    - The scale on the Cauchy distribution for mu [0.8]
% 
%    Prior.Lambda : shape
%                   rate (inverse scale)
%                   lambda 
%   
%    Prior.Z.do   : sample [bool]
%    Prior.Z.DelayedRejection.do : bool
% 
%  For a full description of the model and inference procedure, see Chapter 5 of 
%  Colclough, G. L. "Methods for modelling human functional brain networks with fMRI and MEG," DPhil thesis, University of Oxford, Oxford, UK (2016).


% All internal variables in the model will be unwrapped to save memory,
% save running score of Omega_s and Sigma_s


%	Key References for the approach used in this model: 
%   a combination of the variable substitution in 
%   Wang, H. (2012) ‘Bayesian graphical lasso models and e cient posterior computation.’ Bayesian Analysis, 7(2), 771–790.
%   and the linear regression algorithms in 
%   Peltola, T., Marttinen, P. & Vehtari, A. (2012) ‘Metropolis-Hastings al- gorithm for variable selection in genome-wide association analysis.’ PLoS ONE, 7(11), e49 445.

%	Copyright 2015 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 805 $
%	$LastChangedDate: 2016-03-16 17:32:42 +0000 (Wed, 16 Mar 2016) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 03-Sep-2015 10:33:47















%% Declarations of global parameters
%  
% Global parameters are ugly in Matlab, but this was an easy way to keep
% track of the model
% 
% Global parameters:
% - DEBUG
% - Z_warmup
% - sigma_sq_warmup
% - TIME_SAMPLES
% - SAMPLING_STAGE
% - FIRST_STAGE_LENGTH
% - WARMUP_TIME
% - SAMPLING_TIME
% - sigma_sq_fudge

global sigma_sq_fudge % a parameter which ignores values drawn from prior
                      % allowing proposal distributions to ignore when models go in and
                      % out
%%% Program control
global DEBUG
DEBUG = false;
if DEBUG, fprintf('Debug mode on. \n');   end

% Define a set of sampling stages in warmup
% 1 - full model. 2 - start of sparse sampling using 1 edge at a time. 3 - tune sparse sampling. 4 - Finesse
global SAMPLING_STAGE FIRST_STAGE_LENGTH 
SAMPLING_STAGE     = 0;
FIRST_STAGE_LENGTH = 1000; % samples - 1000 is a fairly good number for the data I've tried

% start the program off in a default state
HIPPO.sampler.warmup.set_sampling_stage(nWarmup, 0);


%%% Time reporting
% an option spit out how long it takes to draw 100 samples once we're warmed up
global TIME_SAMPLES 
TIME_SAMPLES = false;

% time how long the warmup and sampling phases take for feedback reporting
global WARMUP_TIME SAMPLING_TIME 
[WARMUP_TIME, SAMPLING_TIME] = deal(0);

%%% Useful sampling variables to monitor acceptance in the warmup phase
global Z_warmup sigma_sq_warmup;


%% Initialise the random seed
if nargin < 10 || ~exist('rndSeed', 'var') || isempty(rndSeed),
	rndSeed = sum(clock*100);
end%if
try
	% relatively new in matlab, and more efficient
	rng(rndSeed, 'simdTwister');
catch % old Matlab will come here
	rng(rndSeed, 'twister');
end

%% Declare memory
if isempty(saveFileName),
	saveToFile  = false;
	saveFile    = [];
else
	saveToFile  = true;
end%if

[nSamplesTotal, ...
 p,             ... % number of modes
 nSubjects,     ... % 
 nEdges,        ...
 Omega_s_save,  ... % Subject precision matrices
 Sigma_s_save,  ... % Subject covariance matrices
 mu_save     ,  ... % Group level edge strengths
 Z_save      ,  ... % edge presence indicator variables
 chi_sq_save ,  ... % variance parameter
 sigma_sq_save, ... % between-subject variability on edges
 lambda_save ,  ... %
 a_save      ,  ... % sparsity parameter
 lp_save     ,  ... % log probability of model
 p_save      ,  ... % probability of an edge (closely related to Z)
 accept_save,   ... % acceptance rate of transitions in Z
 accept,        ...
 acceptSigma,   ... % acceptance rate of MH kernel on sigma_sq
 accept_gamma,  ... 
 acceptSigma_save, ...
 Z_warmup,         ...
 sigma_sq_warmup] = HIPPO.sampler.initialise.declare_memory(nWarmup, nSamples, S, thin, saveWarmup, saveToFile);

if saveToFile,
	saveFile    = HIPPO.fileio.setup_sample_file(saveFileName, 1, mfilename, nSubjects, p, nEdges, nWarmup, nSamples, saveWarmup, thin, rndSeed);
	easyCleanup = onCleanup(@() HIPPO.fileio.finish_sample_file(saveFile, WARMUP_TIME, SAMPLING_TIME));
end%if

% create a set of indices to relate unravelled symmetric matrices to ravelled. 
% edgeInds has, in each column, the indices for that respective column of a
% symmetric matrix.
edgeIndsLogical = false(nEdges, p);
for i = p:-1:1,
    edgeInds(:,i) = HIPPO.utils.edge_inds(p,i);
	edgeIndsLogical(edgeInds(:,i),i) = true;
end%if

% create a set of indices to pull out sub-matrices from a square matrix
partitionSet = ~eye(p);

%% Initialise
[iSave, ...
 initialSparsity, ...
 Sigma, ...
 Omega_s, ...
 mu, ...
 xi, ...
 eta, ...
 geomDistParam, ...
 edgeSampleWeights, ...
 samplerParams, ...
 Z, ...
 edgeProb, ...
 sigma_sq, ...
 sigma_sq_fudge] = HIPPO.sampler.initialise.initialise(S, p, N, nSubjects, Prior, nWarmup, nEdges);
 


%% Run sampler
% show how we're getting along
reportEvery          = 100; % number of samples before display
adjustWarmupEvery    = 100; % number of samples on which to adjust warmup run

for iSample = 1:nSamplesTotal,
	
	% timing
	if 1 == iSample && nWarmup > 0,
		WARMUP_START = tic;
	elseif iSample == nWarmup + 1,
		SAMPLING_START = tic;
	end%if
	if TIME_SAMPLES % used for characterising speed for paper
		if iSample == nWarmup + 10,
			TSTART = tic;
		end
		if iSample == nWarmup + 110,
			TSTOP = toc(TSTART);
			fprintf('\n\n\n\n100 samples took %f seconds. \n\n\n', TSTOP);
		end
	end
		
	
    % progress reporting
    if(mod(iSample, reportEvery) == 0),		
        if DEBUG,
            fprintf('Sample %d:    Mean Z accept, %0.2f, mean sigma accept %0.2f\n', ...
				    iSample, nanmean(accept_save) * p,    ... % counts roughly one accept if any variable changes in Z
                    nanmean(acceptSigma_save));
		else
			fprintf('    MCMC iter = %d out of %d\n', ...
                    iSample, nSamplesTotal);
        end%if
    end%if
	
	% warmup operations
	if mod(iSample, adjustWarmupEvery) == 1,
		HIPPO.sampler.warmup.set_sampling_stage(nWarmup, iSample);
		if (1 + FIRST_STAGE_LENGTH) == iSample && 2 == SAMPLING_STAGE,
			% initialise Z
			if ~Prior.Z.do,
				Z = true(size(mu));
			elseif ~isempty(Prior.Z.initialGuess),
				Z = Prior.Z.initialGuess;
			else
				Z = HIPPO.sampler.initialise.set_Z_from_sparsity(mu, initialSparsity);
			end%if
		end%if
		if iSample <= nWarmup,
			geomDistParam     = HIPPO.sampler.warmup.set_geom_dist_param(iSample, nWarmup, Prior, geomDistParam, accept_gamma);
			edgeSampleWeights = HIPPO.sampler.warmup.set_edge_sampling_weights(iSample, nWarmup, nEdges, Prior, Z_warmup);
			samplerParams     = HIPPO.sampler.warmup.set_sigma_sq_sampler(iSample, nWarmup, samplerParams, p, sigma_sq_warmup, acceptSigma_save);
		end%if
	end
	
	% start of sampling operations
	if 5 == SAMPLING_STAGE && iSample == nWarmup + 1 && nWarmup > 0,
		% there was a warmup, and we've just started sampling
		geomDistParam     = HIPPO.sampler.warmup.set_geom_dist_param(iSample, nWarmup, Prior, geomDistParam, accept_gamma);
		edgeSampleWeights = HIPPO.sampler.warmup.set_edge_sampling_weights(iSample, nWarmup, nEdges, Prior, Z_warmup);
		samplerParams     = HIPPO.sampler.warmup.set_sigma_sq_sampler(iSample, nWarmup, samplerParams, p, sigma_sq_warmup, acceptSigma_save);
		if DEBUG,
			fprintf('Adjusting sampling: l = %d. \n', geomDistParam);
		end%if
	end%if
	
    % slice Omega for easier access
    Omega_s_edges = HIPPO.utils.get_edges(Omega_s);
    
    % 1. Draw a, hyper prior on edge inclusion
    nEdgesIn = sum(Z);
    a        = HIPPO.sampler.sample_a(Prior.Beta, nEdges, nEdgesIn);
    
    % 2. Draw sigma_eta sq, shared scaling parameter over edges
    sumEtaSq     = sum(eta(Z).^2);
    sigma_eta_sq = HIPPO.sampler.sample_sigma_eta(Prior.Edges.A, nEdgesIn, sumEtaSq);
    
    if DEBUG,
        assert(~isinf(sigma_eta_sq));
    end
    
    % 3. Draw auxilliary variable xi
    sumOmegaS = sum(Omega_s_edges,2);
    xi        = HIPPO.sampler.sample_xi(sumOmegaS, Z, eta, sigma_sq, nSubjects);
    
    if DEBUG,
        assert(~isnan(xi) && ~isinf(xi));
    end%if
    
    % 4. Sample diagonal regularisation
    for iS = nSubjects:-1:1,
        sumDiagOmegaS(iS) = sum(diag(Omega_s(:,:,iS)));
    end%for
    lambda = HIPPO.sampler.sample_lambda(Prior.Lambda, p, sumDiagOmegaS);
    
    % 5. Undo substitution
    chi_sq = xi^2 * sigma_eta_sq;
    
    % 6. Set up loop over columns
    %    A lot of the operations are included in the function body, rather than as separate functions, 
    %    to prevent copying of large matrices by Matlab. This is worth the readability sacrifice. 

    %     a) iterate in random order over columns
    columnOrder = randperm(p);
    
    for jCol = columnOrder,
        % b) create various important subsets
        blockPartition = partitionSet(:, jCol);
        for iS = nSubjects:-1:1,
            Omega_s_11_inv(:,:,iS) = Sigma(blockPartition, blockPartition, iS)                           ...
                                     - Sigma(blockPartition, jCol, iS) * Sigma(jCol, blockPartition, iS) ...
                                     ./ Sigma(jCol, jCol, iS);
        end%for
        S_22           = S(jCol, jCol, :);
        S_12           = S(blockPartition, jCol, :);
        M_s            = bsxfun(@times, Omega_s_11_inv, ...
                                S_22 + reshape(lambda, [1 1 nSubjects]));
        gamma          = logical(Z(edgeIndsLogical(:,jCol)));
        sigma_sq_12    = sigma_sq(edgeIndsLogical(:,jCol));
		
		% create marginal beta prior on gamma, conditional on other edge
		% inclusion variables. The purpose is to integrate out a, as much
        % as possible in this column conditional, when doing edge inclusion sampling. 
        % I'm not sure this is strictly legal, but it massively reduces correlations 
        % in the sampling of a and Z, and it doesn't seem to change results. 
		if ~isfield(Prior.Beta, 'a') || isempty(Prior.Beta.a),
			GammaPrior.numerator.a   = Prior.Beta.a_pi + sum(Z) - sum(gamma); % adds on number of edges in model not in this column
			GammaPrior.numerator.b   = Prior.Beta.b_pi + (nEdges - sum(Z)) - (p - sum(gamma)); % adds on number of edges not in model and not in this column
			GammaPrior.denominator.a = Prior.Beta.a_pi;
			GammaPrior.denominator.b = Prior.Beta.b_pi;
		else
			GammaPrior = [];
		end%if
        
        % c) sample edge inclusion variables
        [gamma, accept(jCol), Sigma_s_plus_inv_R, Xi_inv_R, sigma_sq_12, epsilon_plus, orderPlus] = HIPPO.sampler.sparseModel.sample_edge_inclusion(gamma, a, N, sigma_sq_12, M_s, S_12, chi_sq, Prior, edgeSampleWeights, geomDistParam, GammaPrior);
        
        if DEBUG,
            assert(all(sigma_sq_12(gamma)));
        end%if
        D_tau_plus = diag(sigma_sq_12(gamma).^(-1));
        
        % d) sample mean edge strength
        S_12_plus                    = S_12(gamma, :, :);
        eta(edgeIndsLogical(:,jCol)) = HIPPO.sampler.sample_eta(gamma, epsilon_plus, xi, Xi_inv_R);
        
        % e) reform group mean
        mu(edgeIndsLogical(:,jCol)) = xi .* eta(edgeIndsLogical(:,jCol));
        
        % f) sample substituted variable nu
        nu = HIPPO.sampler.sample_nu(N, S_22, lambda);
        
        % g) sample edges in each subject
        if any(gamma),
            for iS = nSubjects:-1:1,
                m         = (D_tau_plus * mu(edgeInds(gamma,jCol)) ...              
                               - S_12_plus(:,:,iS));
                r         = Sigma_s_plus_inv_R(:,:,iS) \             ...
                             (Sigma_s_plus_inv_R(:,:,iS)' \ m(orderPlus));                             
                u_s(:,iS) = HIPPO.sampler.sample_u(r, Sigma_s_plus_inv_R(:,:,iS), gamma, orderPlus);
            end%for
        else
            u_s = repmat(0, length(gamma), nSubjects);                     %#ok<RPMT0>
        end%if
        
        % h) undo parameterisation

        % edge inclusion variables
        Z(edgeIndsLogical(:,jCol)) = gamma;
        for iS = nSubjects:-1:1,
            % diagonal element
            Omega_s(jCol, jCol, iS)                   = nu(iS) + (u_s(:,iS)'               ...
                                                                  * Omega_s_11_inv(:,:,iS) ...
                                                                  * u_s(:,iS));
            % subject-level edge strengths
            Omega_s_edges(edgeIndsLogical(:,jCol),iS) = u_s(:,iS);
            Omega_s(jCol, blockPartition, iS)         = u_s(:,iS);
            Omega_s(blockPartition, jCol, iS)         = u_s(:,iS);
            
            % update subject-level covariance
            O11invu                                   = Omega_s_11_inv(:,:,iS) * u_s(:,iS);
            Sigma_12                                  = - O11invu ./ nu(iS);
            Sigma(blockPartition, jCol, iS)           = Sigma_12;
            Sigma(jCol, blockPartition, iS)           = Sigma_12;
            Sigma(jCol, jCol, iS)                     = 1.0 ./ nu(iS);
            Sigma(blockPartition, blockPartition, iS) = Omega_s_11_inv(:,:,iS) ...
                                                        + O11invu * O11invu'   ...
                                                        ./ nu(iS);
        end%for           
            

        % i) sample subject-variability edge variances for those in the
        %    model
        if DEBUG,
			assert(islogical(gamma));
            assert(all(sigma_sq_fudge(edgeInds(gamma,jCol))))
        end%if
        [sigma_sq_12, acceptSigma(jCol)] = HIPPO.sampler.MH_sample_sigma_sq_bound(nSubjects, gamma, M_s,              ...
                                                              mu(edgeIndsLogical(:,jCol)), S_12, samplerParams(jCol), ...
                                                              sigma_sq_12,                                            ...
                                                              sigma_sq_fudge(edgeIndsLogical(:,jCol))); %%%%% THIS INDEXING IS ALSO CRAZY SLOW           
        if Prior.RB.do
            % draw variances for all edges not in model from prior. 
            % This is only necessary if we do Rao-Blackwellization
            sigma_sq_12 = HIPPO.sampler.draw_from_sigma_sq_prior(sigma_sq_12, ~gamma);
        end%if

        % Variables not in the model will have sigma_sq set to 0, rather than drawing them all from the prior.
        % If these variables are proposed to be added to the model, the draw from the prior will happen just in time.
        sigma_sq(edgeIndsLogical(:,jCol)) = sigma_sq_12;
        % maintain a version which is not changed when variables leave the
        % model. Again, this is perhaps not quite perfect for an MH sampler. 
        % Nevertheless, the ability to restart the MH kernel from near the previous value, 
        % rather than from wherever in the prior the fill-in value was drawn, is v. useful
        % Actually, maybe it is fine - it just sets the centrepoint for the proposal distribution
        % acceptance is still based on an MH step.
        sigma_sq_fudge(edgeInds(gamma,jCol)) = sigma_sq_12(gamma);
        
        % j) Compute edge-inclusion probabilities using
        %    Rao-Blackwellization (currently disabled)
        edgeProb(blockPartition,jCol) = HIPPO.sampler.sparseModel.RB_edge_probabilities(gamma, a, sigma_sq_12, M_s, S_12, u_s, chi_sq, Prior, nSubjects);
        
        
        if DEBUG,
            assert(~any(isnan(Sigma(:))));
        end%if
    end%loop over columns
    
    % average edge inclusion probabilities
    edgeProb = 0.5 * (edgeProb + edgeProb');
    
    if DEBUG, % spit out log prob 
        % store cholesky factors of OmegaS - should be a quick way to do this
        % in column-wise updating, but not implemented yet. 
        for iS = nSubjects:-1:1,
            Omega_s_R(:,:,iS) = chol(Omega_s(:,:,iS));
        end%for

        % compute log-probability: extra computation, but useful for debugging
        % start with terms from each subject
        for iS = nSubjects:-1:1,
			if any(Z),
				ZdepTerms   = - 0.5 * sum(log(2 * pi .* sigma_sq(Z)))             ... % sum over edges
                              - 0.5 * sum((Omega_s_edges(Z,iS) - mu(Z,1)).^2 ./ (sigma_sq(Z,1) + eps(0))); % end of 2nd line % this is problematic if sigma dives to null
			else
				ZdepTerms   = 0;
			end%if
            lpSubjTerms(iS) = 0.5 * N(iS) * 2 * sum(log(diag(Omega_s_R(:,:,iS)))) ... % inline logdet
                              - 0.5 * p * N(iS) * log(2*pi)                       ...
                              - 0.5 * sum(sum(S(:,:,iS).*Omega_s(:,:,iS)))  ... % inline trace
                              + p * log(0.5 * lambda(iS))                         ...
                              - 0.5 * lambda(iS) * sum(diag(Omega_s(:,:,iS)))     ... % end 1st line
                              + (Prior.Lambda.shape - 1) * log(lambda(iS))        ... % ignore terms not in lambda
                              - Prior.Lambda.rate * lambda(iS)                    ...
                              + ZdepTerms;
        end%for
		% draw alues for eta and sigma_ij that are not included in the
		% model. We're being slightly naughty here: we're not passing these draws
		% over to the sampler on Z for the next round, just discarding them and drawing again. 
		% The LP is therefore not perfectly tied to the reject/accept
		% behaviour of the MH step. 
		% It doesn't matter for the behaviour or convergence of the sampler, however.
		
		if any(~Z),
			nDrawVars           = sum(~Z);
			eta_prior_draw      = sqrt(sigma_eta_sq) .* randn(nDrawVars,1);
			sigma_sq_prior_draw = HIPPO.sampler.draw_from_sigma_sq_prior(zeros(nDrawVars,1), true(nDrawVars,1));
			priorDrawTerms      = - 0.5 * sum(eta_prior_draw.^2) ./ sigma_eta_sq      ...
				                  + sum(log(normpdf( 0.5 * log(sigma_sq_prior_draw), - log(2), 1)) -log(2) - 0.5 .* log(sigma_sq_prior_draw));
		else
			priorDrawTerms = 0;
		end%if
		
		if ~isfield(Prior.Beta, 'a') || isempty(Prior.Beta.a),
			BetaPriorTerm =    (Prior.Beta.a_pi - 1) * log(a + eps(0)) ...
                             + (Prior.Beta.b_pi - 1) * log(1-a+eps(0));
		else
			BetaPriorTerm = 0; % this part already included below
		end%if	
		
        lp = sum(lpSubjTerms) + sum(Z) * log(a + eps(0)) + sum(~Z) * log(1 - a + eps(0))   ...
                              - 0.5 * sum(Z) * log(2 * pi * sigma_eta_sq)                  ...
                              - 0.5 * sum(eta(Z).^2) / sigma_eta_sq                        ... % end of line 3
                              - 0.5 * xi^2                                                 ...
                              - 0.5 * Prior.Edges.A / sigma_eta_sq                         ...
                              - 1.5 * log(sigma_eta_sq)                                    ...
							  + priorDrawTerms                                             ...
							  + sum(log(normpdf( 0.5 * log(sigma_sq(Z)), - log(2), 1)) -log(2) - 0.5 .* log(sigma_sq(Z))); % normal prior on log-sigma %% This is perhaps a little naughty. The sigma_ij terms which are not in the model are drawn from their priors. We don't bother drawing them to save computations (though perhaps we should - it's not burdensome)


                              % old versions of the prior on sigma_sq look like this:
                              %{
                              % + sum(GC_gampdfln(sqrt(sigma_sq), 2, 0.25) -log(2) - 0.5*log(sigma_sq)); % gamma prior on sigma
                              % + sum(-log(0.5) - log(2) - 0.5 * log(sigma_sq)); % uniform prior on sigma
                              %}
                             
    else
        lp = NaN;
    end%if DEBUG     
   
    
    % save out sampled variables
    if (saveWarmup || iSample > nWarmup) && ~mod(iSample,thin),
        iSave = iSave + 1;
		
		if saveToFile,
			HIPPO.fileio.save_samples(saveFile,                                          ...
						 iSave,                                                          ...
				         HIPPO.utils.vectorise_symmetric_matrix(Omega_s, true, false),   ...
						 HIPPO.utils.vectorise_symmetric_matrix(Sigma, true, false),     ...
						 mu,                                                             ...
						 Z,                                                              ...
						 HIPPO.utils.vectorise_symmetric_matrix(edgeProb, false, false), ...
						 chi_sq,                                                         ...
						 sigma_sq,                                                       ...
						 lambda,                                                         ...
						 a,                                                              ...
						 sum(accept),                                                    ...
						 acceptSigma,                                                    ...
						 lp);
					 
		else        
			Omega_s_save(:,:,iSave) = HIPPO.utils.vectorise_symmetric_matrix(Omega_s, true, false);
			Sigma_s_save(:,:,iSave) = HIPPO.utils.vectorise_symmetric_matrix(Sigma, true, false);
			mu_save(:,iSave)        = mu;
			Z_save(:,iSave)         = Z;
			chi_sq_save(iSave)      = chi_sq;
			sigma_sq_save(:,iSave)  = sigma_sq;
			lambda_save(:,iSave)    = lambda;
			a_save(iSave)           = a;
			lp_save(iSave)          = lp;
			p_save(:,iSave)         = HIPPO.utils.vectorise_symmetric_matrix(edgeProb, false, false);
		end%if saveToFile
    end%if
	
	% useful for online reporting
	accept_save(((iSample-1)*p + 1):(iSample*p))      = accept;
	acceptSigma_save(((iSample-1)*p + 1):(iSample*p)) = acceptSigma;
	
	% record useful things for warmup optimisation
	if iSample <= nWarmup,
		accept_gamma(iSample)      = sum(accept);
		Z_warmup(:,iSample)        = Z;
		sigma_sq_warmup(:,iSample) = sigma_sq_fudge;
	end%if
	
	% clean up these things
	if nWarmup + 3 == iSample, % leave a little leeway
		clear accept_gamma Z_warmup
	end%if
		
	% timing
	if iSample == nWarmup,
		WARMUP_TIME = toc(WARMUP_START);
	elseif iSample == nSamplesTotal,
		SAMPLING_TIME = toc(SAMPLING_START);
	end%if
		
    
end%sampling loop

fprintf('\n\n\nSampling Complete. \n');
fprintf('Warmup took %d seconds.\nSampling took %d seconds.\n', WARMUP_TIME, SAMPLING_TIME);
end%sample




% [EOF]