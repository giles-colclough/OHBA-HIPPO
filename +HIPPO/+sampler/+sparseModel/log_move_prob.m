function q = log_move_prob(zetaStar, gamma, m)
%LOG_MOVE_PROB compute log q(zetaStar | gamma)
%
% LOGQ = LOG_MOVE_PROB(ZETASTAR,  GAMMA, M)
%
% Computes the log probability of a move from one model gamma to another,
% gammaStar, which is specified by combining changes in zetaStar to gamma. 
%
% This is the same as computing log q(zetaStar, gammaStar | gamma) if
% gammaStar is formed from gamma and zetaStar. 
%
% The prob q(k | gamma) is not included, as it cancels top and bottom in
% the move probability. 
%
% See Peltola 2012 eq. 7

% initialise log prob
q = 0;

% we'll build gammaStar as we go, but don't need to output it.
gammaStar = gamma;

for ik = 1:length(zetaStar),
	% Did we have to decide to add or remove?
    if all(gammaStar | gamma) || all(~gammaStar | ~gamma), % none to add or none to remove
        % only one option available,
        pickFactor = 0;
    else
        % 50-50 choice
        pickFactor = -log(2);
    end%if
    
    % was this edge an add or remove?
    add = ~gamma(zetaStar(ik));
    
    % what were the available options?
    if add,
        allowedEdges = ~gamma & ~gammaStar; % originally not in model, and hasn't been added in this proposal, yet.
		logEdgeP     = log(m(zetaStar(ik))) - log(sum(m(allowedEdges)));
	else % remove
        allowedEdges =  gamma &  gammaStar; % originally in model, and not been removed yet.
		logEdgeP     = log(1 - m(zetaStar(ik))) - log(sum(1 - m(allowedEdges)));
    end%if
    
    q = q + pickFactor + logEdgeP;   
    
    % update edge inclusion
    gammaStar(zetaStar(ik)) = add;
end%for
end%log_move_prob