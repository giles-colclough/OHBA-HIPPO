function zeta = form_backward_move_proposal(gammaStar, zetaStar, gamma)
%FORM_BACKWARD_MOVE_PROPOSAL
% 
% Algorithm for reversing the steps in zetaStar, to go from gammaStar back
% to gamma 
%
% ZETA = FORM_BACKWARD_MOVE_PROPOSAL(GAMMASTAR, ZETASTAR, GAMMA)
% 
% or, in delayed rejection, 
%
% ZETAPRIMESTAR = FORM_BACKWARD_MOVE_PROPOSAL(GAMMAPRIME, ZETASTAR, GAMMA)
%
% This code implements the algorithm described in the supplementary
% material of Peltola et al 2012 plos one.
%

%%% 1. Construct intermediate
% initialise zeta_b = zetaStar;
zeta = zetaStar;

% find which elements of zetaStar are additions and which removals, as
% zetaStar just encodes the order of variable flips
add = ~gamma(zetaStar);

% reverse the order of removals
zeta(~add) = flip(zeta(~add));

% set all operations to additions
% (mute)

%%% 2. Form zetaPrimeStar
% zeta = zeta_b;

% convert elements of zeta (zetaPrimeStar) to removals such that it's valid
% from gammaPrime (gammaStar)
add = ~gammaStar(zeta);

% reverse order of removals
zeta(~add) = flip(zeta(~add));
