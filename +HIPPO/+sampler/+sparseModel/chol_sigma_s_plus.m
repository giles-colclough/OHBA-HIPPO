function Sigma_s_plus_inv_R = chol_sigma_s_plus(Sigma_s_inv_plus, nSubjects)
%CHOL_SIGMA_S_PLUS
% calculate upper cholesky factor of inverse of Sigma_s+ for each subject

% This is a costly step in the algorithm. 
% Moving to sparse matrices, and sparse cholesky decompositions, might be wise for the sparse model. 


for iS = nSubjects:-1:1,
    Sigma_s_plus_inv_R(:,:,iS) = chol(Sigma_s_inv_plus(:,:,iS));
end%for
end%chol_sigma_s_plus