function [gammaStar, zetaStar, zeta] = form_move_proposal(k, m, gamma)
%form_move_proposal suggest changes to the edges in play
%
% [gammaStar, zetaStar, zeta] = form_move_proposal(k, m, gamma) defines
%   a proposed edge set gammaStar, achieved by applying the edge flips in
%   zetaStar to gamma, using k flips based on estimated edge inclusion
%   probabilities m.

% declare memory
zetaStar = zeros(k,1);

% set up proposal by equating to current status
gammaStar = gamma;

% assign edges to be flipped
for ik = 1:k,
    % decide whether to add or remove an edge
    if all(gammaStar | gamma),
        % all edges currently present or been flipped. We'll have to take one away.
        add = false;
    elseif all(~gammaStar | ~gamma),
        % only remaining possibility is to add an edge
        add = true;
    else
        % decide whether to add or remove with probability 0.5
        add = (rand(1) > 0.5);
    end%if

    % find set of allowed edges from which to choose
    if add,
        allowedEdges = ~gamma & ~gammaStar; % originally not in model, and hasn't been added in this proposal, yet.
		edgeP        = m ./ sum(m(allowedEdges));
	else % remove
        allowedEdges =  gamma &  gammaStar; % originally in model, and not been removed yet.
		edgeP        = (1 - m) ./ sum(1 - m(allowedEdges));
    end%if
    
    % define cumulative probabilities on each of these edges for inclusion
    edgeP(~allowedEdges) = 0;
    edgePcumulative      = cumsum(edgeP);

    % pick an edge
    zetaStar(ik) = find(rand(1) <= edgePcumulative, 1, 'first');

    % perform edge flip
    gammaStar(zetaStar(ik)) = ~gamma(zetaStar(ik));
end%for

% reverse operation is just literal set of reverse flips
zeta = flipud(zetaStar); % will do the same as form_backward_move_proposal(gammaStar, zetaStar, gamma);

end%form_move_proposal