function [SSigmaS, epsilon_plus, Xi_inv_R] = loads_of_stuff(Sigma_s_plus_inv_R, S_12_plus, nIn, sigma_sq_12_plus, D_tau_plus, chi_sq, nSubjects, orderPlus)
%LOADS_OF_STUFF 
%
% Worker function for the middle of the edge inclusion updates
%
% This function computes a set of useful intermediate matrices. 
% The notation should match that used in the thesis. 

% check for presence of terms in this part of the model
if nIn,
    SSigmaS = repmat(0,1,nSubjects);                                       %#ok<RPMT0>
    for iS = nSubjects:-1:1,
        S_12_ordered                    = S_12_plus(orderPlus, :, iS);
        D_tau_ordered                   = diag(1.0./sigma_sq_12_plus(orderPlus));
        tmp                             = Sigma_s_plus_inv_R(:,:,iS)' \ ...
                                               D_tau_ordered;
        DSigmaD(orderPlus,orderPlus,iS) = tmp' * tmp;
        SigmaTimesS(orderPlus,:,iS)     = Sigma_s_plus_inv_R(:,:,iS) \        ...
                                               (Sigma_s_plus_inv_R(:,:,iS)' \ ...
                                                S_12_ordered);
        SSigmaS(iS)                     = S_12_ordered' * SigmaTimesS(:,:,iS);
    end%for

    epsilon_plus = D_tau_plus * sum(SigmaTimesS, 3);

    Xi_plus_inv  = nSubjects * D_tau_plus + eye(nIn) ./ chi_sq ...
                   - sum(DSigmaD, 3);

    Xi_inv_R     = chol(Xi_plus_inv); % WHAT HAPPENS IF THIS IS NOT POS DEF? -> It's not ideal. Need to re-run the sampler, perhaps with a stricter prior on sigma_sq
else
    SSigmaS                  = repmat(0, [1, nSubjects]);          %#ok<RPMT0>
    [epsilon_plus, Xi_inv_R] = deal(repmat(0, [1, 1, nSubjects])); %#ok<RPMT0>
end%if
end%loads_of_stuff


% tested and this syntax is much slower
%         SSigmaS(iS)                     = sum((Sigma_s_plus_inv_R(:,:,iS)' \ ...
%                                                S_12_ordered).^2);
%                                                