function [gamma, accept, Sigma_s_plus_inv_R, Xi_inv_R, epsilon_plus, sigma_sq_12, orderPlus] = delayed_rejection(gamma, gammaStar, zetaStar, lpNew, lpOld, N, nSubjects, sigma_sq_12, M_s, S_12, chi_sq, mBounded, a, k, q, Sigma_s_plus_inv_R, SSigmaS, Xi_inv_R, epsilon_plus, orderPlus, Prior, GammaPrior)
%DELAYED_REJECTION implements delayed rejection metropolis step
%
% Samples a model (which can include the original) from the space of models
% contained within subsets of the original move proposal. 
%
% The delayed proposal is constructed such that it is guaranteed to be
% accepted. 
%
% There is no probability of selecting the previously proposed model. 

global DEBUG

% if there is only one model to investigate, over the original, then the MH
% step is all that we can do.
if 1 == k,
	accept = 0;
	return
end%if

% We need to find the space of models that we're dealing with 
% In particular, we need to identify the biggest model. 
addStar = ~gamma(zetaStar); % which of the original moves were additions and which removals?
try
	[gammaTest, zetaTest, addTest] = HIPPO.sampler.sparseModel.get_possible_submodels(gamma, gammaStar, zetaStar, addStar, k);
catch
	% we got some weird internal errors with nchoosek in Matlab 2012. Just give up
	% on DR if we hit that.
	accept = 0;
	return
end%try

[~, iFullModel] = max(sum(gammaTest));
nModels         = pow2(k);

% Identify which variables were added
addedVars = (bsxfun(@minus, gammaTest, gamma) > 0);

% draw sigma_sq variables for the full model
sigma_sq_12_prime = sigma_sq_12; % are there zeros in this?
if ~Prior.RB.do,
	% if we are using Rao-Blackwellization, these must already have
	% been drawn
	sigma_sq_12_prime = HIPPO.sampler.draw_from_sigma_sq_prior(sigma_sq_12_prime, addedVars(:,iFullModel));
end%if

% We then need to perform the Cholesky decomposition in each subject for
% each model. This should be possible efficiently with a traversal of the
% model space. For now, just get it running easily. 
LargeOverHead = 50;
for iModel = nModels-1:-1:1,
	if nModels == iModel,
		% we never get here - it's not worth testing this model as it's
		% already been rejected, and we're working from this point.
		lpPrime = lpNew;
		
	elseif 1 == iModel,
		lpPrime = lpOld;
		gammaPrime = gamma;
		Sigma_s_plus_inv_R_test{iModel} = Sigma_s_plus_inv_R;
		
	else
		% have to use cell arrays to handle varying dimensionality
		gammaPrime                                                       = gammaTest(:,iModel);
		[Sigma_s_plus_inv_R_test{iModel}, orderPlus_test{iModel}]        = HIPPO.sampler.sparseModel.update_cholesky_decomposition(Sigma_s_plus_inv_R, M_s, sigma_sq_12_prime, gamma, gammaPrime, addedVars(:,iModel), q, LargeOverHead, nSubjects);
		[SSigmaS_test, epsilon_plus_test{iModel}, Xi_inv_R_test{iModel}] = HIPPO.sampler.sparseModel.loads_of_stuff(Sigma_s_plus_inv_R_test{iModel}, S_12(gammaPrime,:,:), sum(gammaPrime), sigma_sq_12_prime(gammaPrime), diag(sigma_sq_12_prime(gammaPrime).^(-1)), chi_sq, nSubjects, orderPlus_test{iModel}); % no need to store SsigmaS
		lpPrime                                                          = HIPPO.sampler.sparseModel.marginal_log_prob(N, nSubjects, sigma_sq_12_prime(gammaPrime), a, q, gammaPrime, Sigma_s_plus_inv_R_test{iModel}, SSigmaS_test, chi_sq, Xi_inv_R_test{iModel}, epsilon_plus_test{iModel}, GammaPrior);
	end%if
	
	% compute 1st stage backward proposal
	zetaPrimeStar = HIPPO.sampler.sparseModel.form_backward_move_proposal(gammaTest(:,iModel), zetaStar, gamma);
	if DEBUG, assert(length(zetaPrimeStar) == k); end % else a) we've erred and b) we need to account for the assymety in Peltola (8).
	
	gammaPrimeStar                = gammaPrime;
	gammaPrimeStar(zetaPrimeStar) = ~gammaPrime(zetaPrimeStar);
	
	% compute its associated log prob. What a faff!
	addedVarsPrimeStar                                       = ((gammaPrimeStar - gammaPrime) > 0);
	newVarsForSigma                                          = ((gammaPrimeStar - logical(sigma_sq_12_prime)) > 0);
	if ~Prior.RB.do,
		% if we are using Rao-Blackwellization, these must already have
		% been drawn
		sigma_sq_12_prime = HIPPO.sampler.draw_from_sigma_sq_prior(sigma_sq_12_prime, newVarsForSigma);

	end%if
	[R, orderPrimeStar]                                      = HIPPO.sampler.sparseModel.update_cholesky_decomposition(Sigma_s_plus_inv_R_test{iModel}, M_s, sigma_sq_12_prime, gammaPrime, gammaPrimeStar, addedVarsPrimeStar, q, k, nSubjects);
	[SSigmaSPrimeStar, epsilonPrimeStar, Xi_inv_R_PrimeStar] = HIPPO.sampler.sparseModel.loads_of_stuff(R, S_12(gammaPrimeStar,:,:), sum(gammaPrimeStar), sigma_sq_12_prime(gammaPrimeStar), diag(sigma_sq_12_prime(gammaPrimeStar).^(-1)), chi_sq, nSubjects, orderPrimeStar); 
	lpPrimeStar                                              = HIPPO.sampler.sparseModel.marginal_log_prob(N, nSubjects, sigma_sq_12_prime(gammaPrimeStar), a, q, gammaPrimeStar, R, SSigmaSPrimeStar, chi_sq, Xi_inv_R_PrimeStar, epsilonPrimeStar, GammaPrior);
	
	% compute prob of a 1st-stage move to gammaPrimeStar, and the probability
	% of reject that move. This is designed such that all terms in Peltola
	% eq.8 cancel.
	[log_q1_back, log_reject] = prob_reject_hypothetical_back_MH_step(zetaPrimeStar, gammaPrime, gammaPrimeStar, lpPrime, lpPrimeStar, mBounded);
    log_q_unnorm(iModel,1)    = lpPrime + log_q1_back + log_reject;
end%for

% normalise jump distribution - no need to test first proposal which has
% already been rejected - it shouldn't even be in the vector
jumpProbs = exp(log_q_unnorm - logsumexp(log_q_unnorm));

% compute delayed rejection move
pickModel = find(rand(1) < cumsum(jumpProbs), 1, 'first');

if isempty(pickModel) || isnan(pickModel) 
	% that's weird. We shouldn't be here. 
	warning([mfilename ':EmptyModel'], ...
		    '%s: Unexpected lack of model choice. This should be checked out. \n', ...
			mfilename);
	% just revert to the old value
	pickModel = 1;
end

% and output chosen model
if 1 == pickModel,
	% we're staying with the old value
	accept = 0;
	% other values are unchanged
	
else
	% overwrite for output
	accept             = 1;
	gamma              = gammaTest(:,pickModel);
	Sigma_s_plus_inv_R = Sigma_s_plus_inv_R_test{pickModel}; % have to use cell arrays as the contents are of varying dimension
	Xi_inv_R           = Xi_inv_R_test{pickModel};
	epsilon_plus       = epsilon_plus_test{pickModel};
	sigma_sq_12        = sigma_sq_12_prime .* gamma; % is this multiplication necessary?
	orderPlus          = orderPlus_test{pickModel};
end%if

end%delayed_rejection

function [qForward, log_reject] = prob_reject_hypothetical_back_MH_step(zetaPrimeStar, gammaPrime, gammaPrimeStar, lpPrime, lpPrimeStar, mBounded)
%PROB_REJECT_HYPOTHETICAL_BACK_MH_STEP
%
% log(prob move to gammaPrimeStar from gammaPrime) + log(1 - accept prob of such a move)

% 'forward': from gammaPrime (second step proposal) to gammaPrimeStar
% (intermediary backwards proposal). Note that q(k) cancels, so this is not
% included.
qForward  = HIPPO.sampler.sparseModel.log_move_prob(zetaPrimeStar, gammaPrime, mBounded);

% 'backward': from gammaPrimeStar to gammaPrime.
zetaPrimeStarReversed = flip(zetaPrimeStar); % should be same result as form_backward_move_proposal(gammaPrimeStar, zetaPrimeStar, gammaPrime);
qBackward             = HIPPO.sampler.sparseModel.log_move_prob(zetaPrimeStarReversed, gammaPrimeStar, mBounded);

% choose whether to accept using MH step
logR       = lpPrimeStar + qBackward - lpPrime - qForward;
log_reject = log1mexp(logR);
end%prob_reject_hypothetical_back_MH_step




% [EOF]