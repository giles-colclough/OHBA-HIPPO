function [R_star, orderPlusStar] = update_cholesky_decomposition(R, M_s, sigma_sq_12_star, gamma, gammaStar, addedVars, q, k, nSubjects)
%UPDATE_CHOLESKY_DECOMPOSITION using a series of rank-1 updates
%
% [R_STAR, ORDER] = UPDATE_CHOLESKY_DECOMPOSITION(R, M_S, SIGMA_SQ_12_STAR, GAMMA, GAMMASTAR, ADDEDVARS, Q, K, NSUBJECTS)
%
% updates cholesky decomposition of inverse of Sigma_s+, after variables
% are added or removed to the model. This function is used for the first proposal stage.
%
% Star suffixes to variables indicate that they are part of the MH proposal
% R - right cholesky factor of the inverse of Sigma_s_plus, which is the block matrix 
%     remaining in the subject's covariance matrix after the current row and column are removed.
%
% M_s, sigma_sq - model variables relating to the current sampling step
% gamma - edge inclusion indicator variable
% addedVars - the edges added in gammaStar on top of those in gamma
% q     - size of the linear regression we're currently doing (nNodes - 1)
% k     - number of edges we're changing in the MH proposal
% order - a re-ordering of variables in the matrices. This is used because it's easiest 
%         to add variables onto the bottom of the cholesky decomposition, and we don't 
%         want to have to recompute as we rotate all variables into their proper locations.
% 
%
% The key point is that low-rank updates to the cholesky decomposition tend
% to be faster than recomputing the whole thing. 

% Even better speed could be achieved by:
% - using sparse matrices to improve cholesky decompositions

global DEBUG

nStar = sum(gammaStar); % number of variables we want to end up with

% there is a limit at which it's faster just to recompute the whole
% decomposition, rathen than doing a series of low-rank updates
if (nStar <= k),
    Sigma_s_inv_plus_star   = bsxfun(@plus, M_s(gammaStar, gammaStar, :), ...
                                            diag(1.0 ./ sigma_sq_12_star(gammaStar)));
    R_star                  = HIPPO.sampler.sparseModel.chol_sigma_s_plus(Sigma_s_inv_plus_star, nSubjects);
    orderPlusStar           = (1:nStar)';
    return
end%if


% remove variables using matlab's qrdelete functionality
removedVars = ((gamma - gammaStar) > 0);

if any(removedVars)
    % where are the variables to be removed in the original R matrix?
    removedVarList = find(removedVars(gamma));
    for iS = nSubjects:-1:1,
        R_del = R(:,:,iS);
        for iR = length(removedVarList):-1:1,
            % work backwards: I think it's easier to perform rotations from
            % bottom up, and easier to index
            remRow       = removedVarList(iR);
            [~,R_del]    = qrdelete(R_del', R_del, remRow);
            R_del(end,:) = []; % remove last row of zeros
        end%while removing variables
        R_new(:,:,iS) = R_del;
    end%for
else
    % no variables to remove
    R_new = R;
end%if

% compute final ordering vector, which is helpful for the next section
order         = (1:nStar)';
moveDown      = addedVars(gammaStar);
orderPlusStar = [order(~moveDown); order(moveDown)];

% add variables in turn onto the bottom of the cholesky factor
if any(addedVars),
    addedVarList = find(addedVars);
    % startVars    = ((gamma - removedVars) > 0);
    nStartVars   = sum((gamma - removedVars) > 0);
    for iS = nSubjects:-1:1,
        R_add = repmat(0, nStar, nStar);                                   %#ok<RPMT0>
        R_add(1:nStartVars,1:nStartVars) = R_new(:,:,iS);
        % existingVars = startVars;
        nVars        = nStartVars;
        
        for iA = 1:length(addedVarList),
            % compute new terms in cholesky factor
            addCol     = addedVarList(iA);
            b          = M_s(gammaStar, addCol, iS);
            newCol     = R_add(1:nVars, 1:nVars)' \ b(orderPlusStar(1:nVars),1);
            schurCompl = M_s(addCol, addCol, iS) + 1.0 ./ sigma_sq_12_star(addCol) - sum(newCol.^2);
            
            if DEBUG,
                assert(schurCompl >= 0);
            end%if
            
            % adjust book-keeping variables
            % existingVars(addCol)   = 1;
            nVars                  = nVars + 1;
            
            % grow the cholesky factor - remember it is upper triangular
            R_add(1:(nVars-1), nVars) = newCol;
            R_add(nVars, nVars)       = sqrt(schurCompl);
        end%adding variables
        R_star(:,:,iS) = R_add;
    end%loop over subjects
else
    R_star = R_new;
end%if


end%update_cholesky_decomposition