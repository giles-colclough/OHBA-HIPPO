function [gammaTest, zetaTest, addTest] = get_possible_submodels(gamma, gammaStar, zetaStar, addStar, k)
%GET_POSSIBLE_SUBMODELS for delayed rejection step
%
% This outlines all the models which can be formed using subsets of the
% alterered variables in zetaStar. 
%
% Delayed rejection (See Peltola 2012) involves picking from a subset of 
% models. This set is all those formed using combinations of the alterations
% proposed in zetaStar. All these various models need to be computed, for later testing. 

nModels         = cumsum(arrayfun(@(i) nchoosek(k,i), 0:k));
nModelsTotal    = nModels(end); % 2^k

gammaTest           = repmat(gamma, 1, nModelsTotal); 
[zetaTest, addTest] = deal(cell(1, nModelsTotal));

% Loop over number of changes to original model
for iVar = 0:k,
	if 0 == iVar,
		% original model
		gammaTest(:,1) = gamma;
		zetaTest{1}    = [];
		addTest{1}     = [];
		
	elseif k == iVar,
		% proposed model after first step
		gammaTest(:,nModelsTotal) = gammaStar;
		zetaTest{nModelsTotal}    = zetaStar;
		addTest{nModelsTotal}     = addStar;
		
	else
		% other models formed from subset of operations
		combs = nchoosek(zetaStar, iVar)'; % each combination is now a new column
		
		for kk = nModels(iVar) + 1 : nModels(iVar+1),
			kComb = kk - nModels(iVar);
			
			zetaTest{kk}                              = combs(:,kComb);
			[gammaTest(zetaTest{kk},kk), addTest{kk}] = deal(~gamma(zetaTest{kk}));
		end%for
	end%if
end%for
end%get_possible_submodels