function lp = marginal_log_prob(N, nSubjects, sigma_sq_plus, a, q, gamma, Sigma_s_plus_inv_R, SSigmaS, chi_sq, Xi_inv_R, epsilon_plus, GammaPrior)
%MARGINAL_LOG_PROB computes log evidence for the model specified by gamma
%
% This is a key worker function used in the M-H updates to edge inclusion
% parameters, doing the computations for a Bayesian Model Averaging scheme.
% 

nIn = sum(gamma);

% We can integrate out the sparsity parameter a, but need to use
% information about the number of edges not in this column.
if ~isempty(GammaPrior),
	logGammaPrior =   betaln(GammaPrior.numerator.a,   GammaPrior.numerator.b) ...
					- betaln(GammaPrior.denominator.a, GammaPrior.denominator.b);
else
	% or, we can just condition on the parameter a.
	logGammaPrior = nIn * log(a) + (q - nIn) * log(1-a);% care over log(0) and log(1)
end%if

if nIn, % check for terms in the model
    for iS = nSubjects:-1:1,
        log_det_Sigma_s_plus(iS) = - 2 * sumlog_base(diag(Sigma_s_plus_inv_R(:,:,iS)));
    end%for

    lp = - 0.5 * sum(N) * log(2*pi())                ...
         - 0.5 * nSubjects * sum(log(sigma_sq_plus)) ... % inline logdet(D_tau)
         + logGammaPrior                             ... 
         + 0.5 * sum(log_det_Sigma_s_plus)           ...
         + 0.5 * sum(SSigmaS)                        ...
         - 0.5 * 2 * sum(log(diag(Xi_inv_R)))        ... % inline logdet(Xi)
         - 0.5 * nIn * log(chi_sq)                   ...
         + 0.5 * sum((Xi_inv_R' \ epsilon_plus).^2);     % inline e' * Xi * e
else
    lp = - 0.5 * sum(N) * log(2*pi()) + logGammaPrior; 
end%if
end%marginal_log_prob