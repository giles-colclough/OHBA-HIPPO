function [gamma, accept, Sigma_s_plus_inv_R, Xi_inv_R, sigma_sq_12, epsilon_plus, orderPlus] = sample_edge_inclusion(gamma, a, N, sigma_sq_12, M_s, S_12, chi_sq, Prior, m, l, GammaPrior)
%SAMPLE_EDGE_INCLUSION
%
% This is the function which runs the M-H sampler for edge inclusion within
% a column. 
%
% Goes through proposing a set of variables to remove / include, then
% computes the evidence for the proposed model, then takes an MH decision. 
% (Note that the MH decision will be accepted more readily than a Gibbs
% step if only one variable is proposed at a time.)
%
% If the model is rejected, we can also do a delayed rejection step which
% is deterministically constructed and necessarily accepted. 
%

global DEBUG


q                = length(gamma);

if ~Prior.Z.do,
    % Ignore sampling on Z.
    % Output all true, for now.
    gamma  = true(q,1);
end

nSubjects        = size(M_s, 3);
D_tau_plus       = diag(sigma_sq_12(gamma).^(-1));
Sigma_s_inv_plus = bsxfun(@plus, M_s(gamma, gamma, :), D_tau_plus);
nIn              = sum(gamma);
orderPlus        = (1:nIn)'; % maintain an ordering variable for computations involving cholesky decompositions

% calculate relevant parameters for log prob
Sigma_s_plus_inv_R                = HIPPO.sampler.sparseModel.chol_sigma_s_plus(Sigma_s_inv_plus, nSubjects);
[SSigmaS, epsilon_plus, Xi_inv_R] = HIPPO.sampler.sparseModel.loads_of_stuff(Sigma_s_plus_inv_R, S_12(gamma,:,:), nIn, sigma_sq_12(gamma), D_tau_plus, chi_sq, nSubjects, orderPlus);
    
if ~Prior.Z.do,
    % Ignore sampling on Z.
    % Output all true, for now.
    % gamma  = true(q,1);
    accept = 0;
    return
	
elseif 1 == a,
	% all variables are in from prior -- wack them all in!
	gamma  = true(q,1);
	accept = 0;
	return
	
elseif 0 == a,
	% all variables are out
	gamma  = false(q,1);
	accept = 0;
	return
	
else
    % M-H sampling of edge inclusion under Beta-Bernoulli prior

    % maintain an estimate of edge inclusion probabilities - now passed in
    % m = repmat(0.5, q, 1);
	mBounded = max(min(m, 0.9), 0.1); % bound edge inclusion probs between 0.1 and 0.9

    % adaptible parameter for geommetric distriubution - now passed in
    % l = 0.8;

    % how many edges are we changing?
    % sample move size from geometric distribution with mode 1 and truncated
    % at the number of variables, or 20.
    maxK = min(20, q);
    k    = min(maxK, geornd(l, [1, 1]) + 1);

    % define a proposal
    [gammaStar, zetaStar, zeta] = HIPPO.sampler.sparseModel.form_move_proposal(k, mBounded, gamma);

    % find probabilities of move in forward and reverse directions
    qForward  = HIPPO.sampler.sparseModel.log_move_prob(zetaStar, gamma, mBounded);
    qBackward = HIPPO.sampler.sparseModel.log_move_prob(zeta, gammaStar, mBounded);
    
    % if necessary, draw values of sigma from the prior
    sigma_sq_12_star = sigma_sq_12;
    addedVars        = ((gammaStar - gamma) > 0);
    if ~Prior.RB.do,
        % if we are using Rao-Blackwellization, these must already have
        % been drawn
        sigma_sq_12_star = HIPPO.sampler.draw_from_sigma_sq_prior(sigma_sq_12_star, addedVars);
    end%if
    D_tau_plus_star  = diag(sigma_sq_12_star(gammaStar).^(-1));
    
    % calculate updated parameters of interest
    [Sigma_s_plus_inv_R_star, orderPlusStar]         = HIPPO.sampler.sparseModel.update_cholesky_decomposition(Sigma_s_plus_inv_R, M_s, sigma_sq_12_star, gamma, gammaStar, addedVars, q, k, nSubjects);
    [SSigmaS_star, epsilon_plus_star, Xi_inv_R_star] = HIPPO.sampler.sparseModel.loads_of_stuff(Sigma_s_plus_inv_R_star, S_12(gammaStar,:,:), sum(gammaStar), sigma_sq_12_star(gammaStar), D_tau_plus_star, chi_sq, nSubjects, orderPlusStar);


    % We can check that our code for rank-1 updates of the cholesky function have worked, using the snippet below. 
    %{
    % Sigma_s_inv_plus_star = bsxfun(@plus, M_s(gammaStar, gammaStar, :), ...
    %                                       D_tau_plus_star);
    % Sigma_s_plus_inv_R_star_check = chol_sigma_s_plus(Sigma_s_inv_plus_star, nSubjects);
    % [SSigmaS_star_check, epsilon_plus_star_check, Xi_inv_R_star_check] = loads_of_stuff(Sigma_s_plus_inv_R_star_check, S_12(gammaStar,:,:), sum(gammaStar), sigma_sq_12_star(gammaStar), D_tau_plus_star, chi_sq, nSubjects, (1:sum(gammaStar))');
    %}


   
    % find model evidence under new proposal
    lpNew = HIPPO.sampler.sparseModel.marginal_log_prob(N, nSubjects, sigma_sq_12_star(gammaStar), a, q, gammaStar, Sigma_s_plus_inv_R_star, SSigmaS_star, chi_sq, Xi_inv_R_star, epsilon_plus_star, GammaPrior);
    lpOld = HIPPO.sampler.sparseModel.marginal_log_prob(N, nSubjects, sigma_sq_12(gamma),          a, q, gamma,     Sigma_s_plus_inv_R,      SSigmaS,      chi_sq, Xi_inv_R,      epsilon_plus,      GammaPrior);
    
    if DEBUG,
        assert(~isnan(lpNew) && ~isnan(lpOld) && ~isinf(lpNew) && ~isinf(lpOld));
    end%if
    
    % choose whether to accept using MH step
    logR       = lpNew + qBackward - lpOld - qForward;
    logpAccept = min(0.0, logR);
    
    accept = (log(rand(1)) <= logpAccept);
end%if

if accept,
    % substitute in the new variables
    gamma              = gammaStar;
    Sigma_s_plus_inv_R = Sigma_s_plus_inv_R_star;
    Xi_inv_R           = Xi_inv_R_star;
    epsilon_plus       = epsilon_plus_star;
    sigma_sq_12        = sigma_sq_12_star; % .* gammaStar;
    orderPlus          = orderPlusStar;
	
elseif Prior.Z.DelayedRejection.do,
	% have another bite. 
	% We want to overwrite the accept variable to indicate if a change has
	% been made in this step. 
	[gamma, accept, Sigma_s_plus_inv_R, Xi_inv_R, epsilon_plus, sigma_sq_12, orderPlus] = HIPPO.sampler.sparseModel.delayed_rejection(gamma, gammaStar, zetaStar, lpNew, lpOld, N, nSubjects, sigma_sq_12, M_s, S_12, chi_sq, mBounded, a, k, q, Sigma_s_plus_inv_R, SSigmaS, Xi_inv_R, epsilon_plus, orderPlus, Prior, GammaPrior);
else
	% reject
	
end%if

gamma = logical(gamma);
end%sample_edge_inclusion

% [EOF]