function p = RB_edge_probabilities(gamma, a, sigma_sq_12, M_s, S_12, u_s, chi_sq, Prior, nSubjects)
%RB_EDGE_probabilities compute edge inclusion probability in a column using
%  Rao-Blackwellization
%
%
% if Prior.RB.do, attempt to do Rao-Blackwellization. This should be a
% series of linear regression steps to better estimate inclusion
% probabilities
%
% If not, then just return the sampled binary vector gamma as the best
% estimate. 

% this approach is based on the maths in Peltola 2012, but it doesn't seem to work very well (nonsensical probabilities)
% in this implementation (I've probably got a bug.) Therefore disable for the time being


global DEBUG

if Prior.RB.do

    error([mfilename ':RBOptionNotAvailable'], 'Rao-Blackwellization not available at the moment. \n')

    %{
    sigma_sq_inv = 1.0 ./ sigma_sq_12;
    
    % compute subject terms
    for iS = nSubjects:-1:1,
        dM             = diag(M_s(:,:,iS));
        deltaInv(:,iS) = 1.0 ./ dM + sigma_sq_inv;
        v(:,iS)        = S_12(:,iS) + M_s(:, gamma,iS) * u_s(gamma, iS) ...
                         - u_s(:,iS) .* dM;
    end%for
    N_chi_delta = nSubjects * sigma_sq_inv + 1.0 ./ chi_sq + sigma_sq_inv .* sum(deltaInv,2);
    log_lambda  = 0.5 * nSubjects * log(sigma_sq_inv) ...
                  + 0.5 * sum(deltaInv,2)   ...
                  + 0.5 * sum(v.^2 .* deltaInv, 2) ...
                  - 0.5 * log(N_chi_delta) ...
                  - 0.5 * log(chi_sq) ...
                  + log(a) - log(1-a) ...
                  + 0.5 * sum(v.^2  .* deltaInv.^2, 2) .* sigma_sq_inv.^2 ./ N_chi_delta;
              
    if log_lambda < -50,
        log1plambda = log1p(exp(log_lambda));
        p           = exp(log_lambda - log1plambda);
    elseif log_lambda < 50
        % care over computing log 1+lambda? No, just go for it
        lambda = exp(log_lambda);
        p      = lambda ./ (1 + lambda);
    else
        % in case values are huge, compute log prob then exponent
        p = 1.0 ./ (1 + exp(-log_lambda));
    end%if
    %}
else
    p = gamma;
end%if

if DEBUG,
    assert(all(~isnan(p) & ~isinf(p)));
end%if DEBUG
end%RB_edge_probabilities