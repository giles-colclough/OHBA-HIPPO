function eta = sample_eta(gamma, epsilon_plus, xi, Xi_inv_R)
%SAMPLE_ETA draw mean edge strengths eta for one column
%
% This is a marginal draw, with parameters below integrated out. 


% edges not in the model should have mean zero, so initialise in that
% fashion
eta = repmat(0, length(gamma), 1);                                         %#ok<RPMT0>

% covariance is passed in as
% Xi_plus_inv = nSubjects * D_tau_plus + eye(sum(gamma)) ./ chi_sq ...
%               - D_tau_plus * sum(Sigma_s_plus,3) * D_tau_plus; 
if any(gamma)
% form mean
mu = - (Xi_inv_R \ (Xi_inv_R' \ epsilon_plus ./ xi)); % inline inv(Xi/xi^2) * eta * xi
                         
% draw from multivariate normal using inline version of cholesky sampler
eta(gamma) = mu + (xi * Xi_inv_R \ randn(length(mu), 1));
end%if
end%sample_eta