function u = sample_u(r, R, gamma, order)
%SAMPLE_U draw edge strengths for a single subject
%
% u ~ N(r, Cov)
%
% U = SAMPLE_U(R, COV_INV_R, GAMMA, ORDER) for mean of normal distribution R,
%   cholesky factor of inverse covariance COV_INV_R, and present model
%   terms gamma. ORDER holds a re-ordering of the variables indicated by gamma, 
%   used to speed up cholesky operations

% initialise elements not in model to zero
u = zeros(length(gamma), 1);

if any(gamma),
    % draw from multivariate normal using inline version of cholesky sampler
    n(order, 1) = R \ randn(length(r), 1); 
    u(gamma)    = r + n;
end%if
end%sample_u