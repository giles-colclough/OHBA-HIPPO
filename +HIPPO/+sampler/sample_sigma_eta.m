function sigma_eta_sq = sample_sigma_eta(A, nEdges, sumEtaSq)
%SAMPLE_ETA_SQ draw shared variance parameter on edges
%
% sigma^2_eta ~ inv-chi^2(nEdges + 1, (A + sum_{i<j} eta^2_{ij})/(1+nEdges))
%
% sigma_eta_sq = SAMPLE_SIGMA_ETA(A, NEDGES, SUMETASQ)
%

nu           = nEdges + 1;
tauSq        = (A + sumEtaSq) ./ (1 + nEdges);
% inv-chi^2 can be parameterised as an inverse-Gamma
sigma_eta_sq = HIPPO.utils.stats.randinvgamma(0.5 * nu, 0.5 * nu * tauSq);
% sigma_eta_sq = 0.5 * nu * tauSq ./ randgamma(0.5 * nu); % inline randinvgamma
end%sample_sigma_eta