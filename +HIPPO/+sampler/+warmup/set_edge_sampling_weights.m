function p = set_edge_sampling_weights(iSample, nWarmup, nEdges, Prior, Z)
%SET_EDGE_SAMPLING_WEIGHTS to alter the preferences over which edges 
%  to choose for inclusion or exclusion
%
% WARMUP FUNCTION
%
% maintains estimate of inclusion probabilities, to weight proposals for
% edges to include or exclude
%
% P = SET_EDGE_SAMPLING_WEIGHTS(I, N, NE, Z)
%
% I - iSample
% N - number of warmup samples
% NE - number of edges in the model
% Z  - saved warmup samples of edge inclusion parameters
% 
% P - edge inclusion probabilities

global SAMPLING_STAGE FIRST_STAGE_LENGTH

if ~Prior.Z.do,
	p = repmat(1, nEdges, 1);                                              %#ok<REPMAT>
	return
end%if

if 0 == iSample || SAMPLING_STAGE <= 2,
	p = repmat(0.5, nEdges, 1); % start with equal probs either way
	
elseif 3 == SAMPLING_STAGE || 4 == SAMPLING_STAGE, 
	% generally, update prior based on the last 1000 samples, unless in stage 5
	movingWindow = FIRST_STAGE_LENGTH; % 1000 samples
	p = nanmean(Z(:,max(FIRST_STAGE_LENGTH,iSample-movingWindow):iSample),2); % It would be better to use a Rao-Blackwellized estimate here.
	
elseif 5 == SAMPLING_STAGE
	stage_4_inds = round(3*nWarmup / 4) : nWarmup;
	p = nanmean(Z(:,stage_4_inds),2);
	
else
	error('Unexpected sampling stage. \n');
end%if

end%set_edge_prior