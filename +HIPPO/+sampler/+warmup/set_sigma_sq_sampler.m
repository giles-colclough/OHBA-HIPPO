function samplerParams = set_sigma_sq_sampler(iSample, nWarmup, samplerParams, nModes, sigma_sq, acceptSigma)
%SET_SIGMA_SQ_SAMPLER sets parameters for the MH sampler on sigma_sq
% SAMPLERPARAMS = SET_SIGMA_SQ_SAMPLER(iSAMPLE, NWARMUP, SAMPLERPARAMS, NMODES, SIGMA_SQ, ACCEPTSIGMA)
% 
%  The sampler operates on the log of sigma_sq, where the posterior has more chance of being normal.
%
% WARMUP FUNCTION
%
% adjusts the MH kernel on sigma_sq, optimising during warmup to achieve a 
% acceptance rate of 0.23
% 
% Optimisation proceeds by setting the sampler based on the dimensionality 
% of the space, using Andrew Gelman's method, then using a bracketing 
% method to optimise further

global SAMPLING_STAGE FIRST_STAGE_LENGTH
movingWindow = FIRST_STAGE_LENGTH/2; % samples - note: shorter than window for Z.
selectInds   = iSample-movingWindow+1:iSample-1;

q = nModes - 1; % size of gamma

if 0 == iSample, % intialise,
	for iCol = nModes:-1:1,
	    samplerParams(iCol).c       = 0.2;
	    samplerParams(iCol).Sigma_L = eye(q); 
	end
elseif 1 == SAMPLING_STAGE && iSample > FIRST_STAGE_LENGTH / 2,
	for iCol = nModes:-1:1,
		samplerParams(iCol).c       = 2.38 ./ sqrt(q); % this does really a remarkable job of getting acceptance to 0.23
	    Sigma                       = update_covariance(sigma_sq, nModes, iCol, selectInds);
		samplerParams(iCol).Sigma_L = update_chol_factor(samplerParams(iCol), Sigma);
	end
	
elseif 2 == SAMPLING_STAGE, 
	% generally, update prior based on the last 1000 samples, unless in stage 5
	c = update_c(samplerParams, acceptSigma, iSample, nModes, movingWindow);
	for iCol = nModes:-1:1,
		samplerParams(iCol).c       = c;
	    Sigma                       = update_covariance(sigma_sq, nModes, iCol, selectInds);
		samplerParams(iCol).Sigma_L = update_chol_factor(samplerParams(iCol), Sigma);
	end
elseif 3 == SAMPLING_STAGE,
	% no adjustments in this window
	
elseif 4 == SAMPLING_STAGE,
	c = update_c(samplerParams, acceptSigma, iSample, nModes, movingWindow);
	for iCol = nModes:-1:1,
		samplerParams(iCol).c       = c;
	    Sigma                       = update_covariance(sigma_sq, nModes, iCol, selectInds);
		samplerParams(iCol).Sigma_L = update_chol_factor(samplerParams(iCol), Sigma);
	end
	
elseif 5 == SAMPLING_STAGE % we only get here once (hopefully)
	stage_4_inds = round(3*nWarmup / 4) : nWarmup;
	for iCol = nModes:-1:1,
	    Sigma                       = update_covariance(sigma_sq, nModes, iCol, stage_4_inds);
		samplerParams(iCol).Sigma_L = update_chol_factor(samplerParams(iCol), Sigma);
	end
	
elseif SAMPLING_STAGE > 5,
	error('Unexpected sampling stage. \n');
else
	% leave as is
end%if
end%set_edge_prior






function c = update_c(samplerParams, acceptSigma, iSample, nModes, window)
% UPDATE_C adjust the scaling of the kernel on sigma_sq
% 
% SamplerParams is current state of sampler on log sigma
% 
global FIRST_STAGE_LENGTH
c             = samplerParams.c;
targetC       = 0.23;
averagePeriod = max(iSample*nModes - (window * nModes) + 1, FIRST_STAGE_LENGTH) : iSample*nModes; % accept values are stored in a pretty dorky way, in a long line, with one value for each column
meanAccept    = nanmean(acceptSigma(averagePeriod));

if meanAccept < 0.18,
	c = 0.92 * c;
elseif meanAccept > 0.3,
	c = 1.08 * c;
elseif meanAccept > targetC,
	c = 1.01 * c;
elseif meanAccept < targetC,
	c = 0.99 * c;
end
end%update_c

function Sigma = update_covariance(sigma_sq, nModes, iCol, window)
ss    = sigma_sq(HIPPO.utils.edge_inds(nModes, iCol),window);
Sigma = nancov(0.5 * log(ss')); % cov( log sigma )
end

function Sigma_L = update_chol_factor(samplerParams, Sigma)
[Sigma_L, fail] = chol(sparse(Sigma), 'lower');
if fail,
	% not positive definite
	% don't bother dealing with machine-precision errors, just use old
	% value
	Sigma_L = samplerParams.Sigma_L;
end
end
	