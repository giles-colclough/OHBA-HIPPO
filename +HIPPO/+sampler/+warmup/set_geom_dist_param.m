function l = set_geom_dist_param(iSample, nWarmup, Prior, l, accept_gamma)
%SET_GEOM_DIST_PARAM sets parameters for proposal distribution in sampler
% 
% L = SET_GEOM_DIST_PARAM(ISAMPLE, NWARMUP, PRIOR, L, ACCEPT_GAMMA)
%   sets the parameter, l, for the geommetric distritbuion that 
%   controls the number of edges that are attempted to be flipped. 
% 
%   It can adapt to reach a target acceptance level.


%
% WARMUP FUNCTION
%
% Sets parameter l of proposal distribution during the warmup phase. 
%
% Attempts to maximise the number of transitions which occur. Uses a grid
% search method to choose the best parameter. 
%
% Note - could use a more appropriate metric like expected squared jump
% distance, but as acceptance is 5-10% of samples for a change on any edge
% at all, we just want to maximise the number of changes. 

global SAMPLING_STAGE FIRST_STAGE_LENGTH
global DEBUG

% define what happens if we're not actualy doing sparse spike and slab sampling
if ~Prior.Z.do,
	l = 0.999;
	return
end%if

% try a grid of possible parameters
nGrid        = min(20, round(nWarmup / 1000)); % change about every 500 samples, or max 20 times
min_l        = 0.6; % 0.2 -> mean of about 5 edges
stage_2_l    = 0.999; % roughly one at a time
max_l        = stage_2_l; % mean of about 1 edge
grid         = fliplr(logspace(log10(min_l), log10(max_l), nGrid));          %#ok<NASGU> - unused if we don't take grid approach
changePoints = linspace(round(nWarmup/4) + 1, round(3*nWarmup/4) + 1, nGrid + 1);
testChunk    = mode(diff(changePoints)); % in samples % what if there is nothing in changePoints?


% perform bracketed search
persistent UPPER_L
persistent LOWER_L
persistent MIDDLE_L
persistent NEXT_CHANGE
persistent score
persistent scoreFillInd 
persistent FINAL_VAL_SET
persistent START_TIME

useBracket = true;

% Change behaviour depending on sampling stage
if 0 == iSample,
	% before sampling starts - set to sensible value
	l = 0.8;
	START_TIME = tic;
	
elseif SAMPLING_STAGE <= 2,
	%  sample only one edge at once
	l = stage_2_l; % roughly one at a time
	
	% get ready for adaptive phase
	UPPER_L       = l;
	LOWER_L       = min_l;
	MIDDLE_L      = 0.5 * (LOWER_L + UPPER_L); 
	NEXT_CHANGE   = 1;
	scoreFillInd  = 3; 
	FINAL_VAL_SET = false;
	
	if iSample == max((1 + FIRST_STAGE_LENGTH), changePoints(1) - testChunk); % take first chunk of bracketting from stage 2
		START_TIME = tic; % start timing
	end%if
	
elseif 3 == SAMPLING_STAGE, % between nWarmup / 4 and 3 nWarnup / 4
	%  search for best l
	if useBracket, 
		if iSample == changePoints(NEXT_CHANGE) || 1 == NEXT_CHANGE,
			
			% catch time. 
			chunkTime = toc(START_TIME);
			
			score(scoreFillInd) = update_score(accept_gamma, iSample, testChunk, chunkTime);
			
			if DEBUG, report(iSample, score, LOWER_L, MIDDLE_L, UPPER_L); end
			
			if 1 == NEXT_CHANGE,
				% first bracketing step
				l            = LOWER_L;
				scoreFillInd = 1;
				
			elseif 2 == NEXT_CHANGE,
				% first midpoint
				l            = MIDDLE_L;
				scoreFillInd = 2;
				
			else
				[l, LOWER_L, MIDDLE_L, UPPER_L, scoreFillInd] = update_bracket(LOWER_L, MIDDLE_L, UPPER_L, score);
				
			end%if
			
			% increment changepoint counter
			NEXT_CHANGE = NEXT_CHANGE + 1;
			
			% set up timer.
			START_TIME = tic;
			
		else
			% keep parameters as they are
		end%if
		
	else % use grid search
		% use histc to count where the sample falls 
		myBin        = histc(iSample, changePoints); 
		l            = grid(logical(myBin(1:end-1)));
	end%if
	
elseif 4 == SAMPLING_STAGE,
	%  Set l and continue sampling. 
	if ~FINAL_VAL_SET,
		if useBracket, 
			% update score final time
			chunkTime = toc(START_TIME);
			score(scoreFillInd) = update_score(accept_gamma, iSample, testChunk, chunkTime);
			
			if DEBUG, report(iSample, score, LOWER_L, MIDDLE_L, UPPER_L); end
			
			[l, bestScore] = set_best_l(score, LOWER_L, MIDDLE_L, UPPER_L);
			report_parameters(l, bestScore);
			
		else % use gridsearch
			% identify best parameter from grid
			for iGridStep = nGrid:-1:1, 
				nAccept(iGridStep) = nansum(accept_gamma(1:nWarmup <= changePoints(iGridStep + 1) & 1:nWarmup > changePoints(iGridStep)));
			end%for
			[~, best_choice] = max(nAccept);
			l                = grid(best_choice);
		end%if
		
		FINAL_VAL_SET = true;
	end%if
	
elseif 5 == SAMPLING_STAGE,
	% keep l as it is.
else
	error('Unexpected sampling stage. \n');
end%if



end%set_geom_dist_param

function s = update_score(accept, iSample, chunkLength, chunkTime)
%UPDATE_SCORE pull out number of accepts from past chunk of samples and
%divide by computation time
global FIRST_STAGE_LENGTH

s = nansum(accept(max(iSample-chunkLength, (1 + FIRST_STAGE_LENGTH)) : iSample-1)) ./ chunkTime;

end%update_score

function [l, maxVal] = set_best_l(score, LOWER_L, MIDDLE_L, UPPER_L)
%SET_BEST_L from adjustment run

lChoice     = [LOWER_L MIDDLE_L UPPER_L];
[maxVal, ~] = max(score); % chooses smallest value in case of tie
lBest       = find(score == maxVal, 1, 'last'); % chooses largest value in case of tie.
l           = lChoice(lBest);
end%set_best_l

function report(iSample, score, LOWER_L, MIDDLE_L, UPPER_L)
% report progress to command line.
[lBest, bestScore] = set_best_l(score, LOWER_L, MIDDLE_L, UPPER_L);
fprintf('Sample %d: Adaptive sampling: best geo dist param = %d, with moves per second = %d. \n', iSample, lBest, bestScore);
end%report
function report_parameters(l, score)
fprintf('\nAdaptive sampling finished. Setting geo dist param = %d, with moves per second = %d. \n', ...
	    l, score);
end%report_parameters

function [CURRENT_L, LOWER_L, MIDDLE_L, UPPER_L, scoreFillInd] = update_bracket(LOWER_L, MIDDLE_L, UPPER_L, score)
%UPDATE_BRACKET updates the bracketing algorithm.
%
% Possible cases
%       -                -
%    -      or               -
% -                  -               low l ----> high l
%
% In the first case, shift the upper / lower bounds and place a new
% middle point
%
% In the second, fill in the largest gap, while retaining the middle
% point. 

[maxVal, ~] = max(score); % chooses smallest value in case of tie
maxInd = find(score == maxVal, 1, 'last'); % chooses largest value in case of tie.

if 1 == maxInd,
	% low l is best
	UPPER_L      = MIDDLE_L;
	MIDDLE_L     = 0.5 * (UPPER_L + LOWER_L);
	CURRENT_L    = MIDDLE_L;
	scoreFillInd = 2; 

elseif 3 == maxInd,
	% upper l is best
	LOWER_L      = MIDDLE_L;
	MIDDLE_L     = 0.5 * (UPPER_L + LOWER_L);
	CURRENT_L    = MIDDLE_L;
	scoreFillInd = 2;

else
	% middle l is best
	gapSize = diff(score);
	[~, biggestGap] = max(gapSize);
	if 1 == biggestGap,
		LOWER_L      = 0.5 * (LOWER_L + MIDDLE_L);
		CURRENT_L    = LOWER_L;
		scoreFillInd = 1;
	else
		UPPER_L      = 0.5 * (UPPER_L + MIDDLE_L);
		CURRENT_L    = UPPER_L;
		scoreFillInd = 3;

	end%if
end%if
end%update_bracket
%#ok<*NODEF> - variables will be properly defined if sampling stages are reached sequentially
%#ok<*UNRCH> - don't warn about unavailable options. We've hard-coded a switch between two algorithm options.
% [EOF]