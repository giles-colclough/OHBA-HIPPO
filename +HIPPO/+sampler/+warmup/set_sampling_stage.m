function set_sampling_stage(nWarmup, iSample)
%SET_SAMPLING_STAGE warmup function to determine operations
% 
%  SET_SAMPLING_STAGE(NWARMUP, ISAMPLE)
%    sets global variables to determine which of 5 sampling stages 
%    we are in, based on the current sample number ISAMPLE and 
%    the total warmup size, NWARMUP. 


%
% WARMUP FUNCTION
%
% There are 5 sampling stages:
% 1 - sample from full model. 1000 samples only
% 2 - initial sampling of the graph, from random sparse initialisation,
%      jumping one edge at a time
% 3 - adaptive phase, altering jumping kernel and proposal weights
% 4 - end of adaption of geometric proposal distribution, but final setting
%      of proposal weights
% 5 - Sampling, no longer warmup. All finite adaption parameters are now
%     fixed. 


global SAMPLING_STAGE WARMUP_TIME FIRST_STAGE_LENGTH

if ~nargin,
	SAMPLING_STAGE = 0;
	return
end%if

old_stage = SAMPLING_STAGE;

%  determine new sampling stage
if iSample > nWarmup || 0 == nWarmup,
	% we're sampling properly
	SAMPLING_STAGE = 5;
elseif iSample <= FIRST_STAGE_LENGTH,
	% sampling from full model
	SAMPLING_STAGE = 1;
elseif iSample <= round(nWarmup/4),
	% initial sampling of graph
	SAMPLING_STAGE = 2;
elseif iSample <= round(3*nWarmup/4),
	% adaptive sampling
	SAMPLING_STAGE = 3;
else
	% final warmup stage
	SAMPLING_STAGE = 4;
end%if

% Check to see if anything's changed
if SAMPLING_STAGE > old_stage,
	fprintf('\nEntering Sampling stage %d. \n', SAMPLING_STAGE);
	if 5 == SAMPLING_STAGE,
		fprintf('Warmup took %d s. \n', WARMUP_TIME);
	end%if
	
elseif SAMPLING_STAGE < old_stage,
	error('we seem to have moved backwards in the sampling process. \n');
else
	%show nothing
end%if
end%set_sampling_stage