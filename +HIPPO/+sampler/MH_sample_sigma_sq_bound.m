function [sigma_sq, accept] = MH_sample_sigma_sq_bound(nSubjects, gamma, M_s, mu, S_12, samplerParams, sigma_sq_old, sigma_sq_fudge_12)
%MH_SAMPLE_SIGMA_SQ draw variances representing subject variability 
% 
% [SIGMA_SQ, ACCEPT] = MH_SAMPLE_SIGMA_SQ_BOUND(NSUBJECTS, GAMMA, M_S, MU, S_12, SAMPLERPARAMS, SIGMA_SQ_OLD, SIGMA_SQ_FUDGE_12)
%
% Uses a Gaussian MH kernel to move around in log space of sigma sq. Returns the new value of Sigma_sq, and a binary variable
%  indicating whether or not the proposed sample was accepted. 
%
% The accept/reject step is calculated based on the old value of sigma_sq, which may include new variables in the model, 
% for which sigma_sq was drawn from its prior. 
% However, the MH jumping kernel is centred on a value for sigma_sq that corresponds to the last time
% that variable was included in the model.

global DEBUG

% initialise to zero, as should be zero for edges not in model
nEdges   = length(gamma); % in the column / model, whatever is passed in
sigma_sq = repmat(0, nEdges, 1);                                           %#ok<RPMT0>
accept   = 0;

if DEBUG,
	% do some input checking
	assert(islogical(gamma));
	nEl = length(gamma);
	assert(length(mu)           == nEl);
	assert(ROInets.rows(M_s)    == nEl);
	assert(ROInets.rows(S_12)   == nEl);
	assert(length(sigma_sq_old) == nEl);
	assert(size(M_s,3)          == nSubjects);
	assert(size(S_12,3)         == nSubjects);
end%if

% define a set of nested functions to simplify the sampling statements

% likelihood statement
    function lp = log_prob(sigma_sq_inv)
        if any(sigma_sq_inv <= 0),
            lp = - realmax;
            return
        end
        for iS = nSubjects:-1:1,
            SSS           = chol(M_s_plus(:,:,iS) + diag(sigma_sq_inv));
            rho           = (sigma_sq_inv) .* mu_plus - S_12_plus(:,:,iS);
            subjTerms(iS) = - 0.5 * 2 * sumlog_base(diag(SSS)) ... % 0.5 * logdet(Sigma+) - this line is quite slow
                            + 0.5 * sum((SSS' \ rho).^2);          % 0.5 * rho' * Sigma+ * rho
        end%for
        lp =  0.5 * nSubjects * sumlog_base(sigma_sq_inv)       ... %   N/2 * logdet(D_tau) - this line is quite slow. ! 
            - 0.5 * nSubjects * sum(sigma_sq_inv .* mu_plus.^2) ... % - N/2 mu' * Dtau * mu
            + sum(subjTerms);
    end%log_prob nested function

% log normal prior on sigma
log_prior = @(sigma_sq_inv) sum(log(normpdf(- 0.5 * log(sigma_sq_inv), - log(2), 1)) + 0.5 .* log(sigma_sq_inv));  

% tested out different priors on sigma_sq - the below were not as good.
%     log_prior = @(sigma_sq_inv) sum(-log(0.5) + - log(2) + 0.5 * log(sigma_sq_inv)) - 1e50 * any(1.0./sqrt(sigma_sq_inv) > 0.5); % uniform prior on sigma, converted to prior on sigma_sq
%     log_prior = @(sigma_sq_inv) sum(GC_gampdfln(1.0./sqrt(sigma_sq_inv), 2, 0.25)- log(2) + 0.5*log(sigma_sq_inv)); % gamma prior on sigma, transformed to prior on sigma_sq

   
% the density we want to sample from 
    function lp = sample_pdf(s)
        sInv     = 1.0 ./ s;
        lp       = log_prob(sInv) + log_prior(sInv);
		
        
        if DEBUG && any(isnan(log_prior(sInv))),
            warning('Prior computation failed');
        end%if
    end%sample_pdf

% start the sampler. We only need to draw values for edges that are in the model.
if any(gamma),
	nPlus           = sum(gamma);
    % setup with old values
    sigma_sq(gamma) = (sigma_sq_old(gamma));
    M_s_plus        = M_s(gamma,gamma,:);
    mu_plus         = mu(gamma); 
    S_12_plus       = S_12(gamma,1,:);
	
    
    % Gaussian kernel on log(sigma)
	if isempty(samplerParams) || ~isstruct(samplerParams) || ~isfield(samplerParams, 'c'),
		scale     = 0.2;
		jumpChol  = eye(nEdges);
	else
		scale    = samplerParams.c;
		jumpChol = samplerParams.Sigma_L;
	end

	% don't bother computing cholesky decomposition for this particular
	% subset. Instead, just sample all of gamma and pass the relevant
	% subset into the acceptance algorithm.
	% This is still a symmetric proposal, so we're fine.
    %
    % The centre of the proposal distribution is based on stored values variables, if they
    % had been taken out of the model in previous rounds. This means we centre the proposal on 
    % sensible values, rather than on wherever was drawn from the broad prior.
    suggestall  = sigma_sq_fudge_12 .* exp(2 * scale .* jumpChol * randn(nEdges,1)); % the 2 makes us move on sigma, not sigma^2
	suggest     = suggestall(gamma);
	
	% implement a hard bound of log(sigma) >= -6
	% this breaks the MH kernel if we get down to this bound, but we're at such small variances it doesn't matter so much. 
	% The key thing is that the sampler can climb out if the likelihood favours it, and return to 'sensible regions'
	% We don't want to reject just because one variate has dropped below
	% zero, but we don't in general want a Gibbs sampler either.	
	boundHits          = log(suggest) < -12;
	suggest(boundHits) = deal(exp(-12)); 
	
    MHadjust = @(Tprime, T) sumlog_base(T(:)) - sumlog_base(Tprime(:)); % adjust with determinant of vector of all parameters
    
    log_r    = min(0, sample_pdf(suggest) - sample_pdf(sigma_sq(gamma)) ...
                      - MHadjust(suggest, sigma_sq(gamma)));
    accept  = log(rand(1)) <= log_r;

    if accept,
        sigma_sq(gamma) = suggest;
    end%if    
end%if
end%MH_sample_sigma_sq