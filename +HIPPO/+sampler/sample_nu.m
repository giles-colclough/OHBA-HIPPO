function nu = sample_nu(N, S_22, lambda)
%SAMPLE_NU draw diagonal element (after cov) in all subjects
%
% nu ~ Ga(N_s/2 + 1, 2 / (S_22 + lambda))
%
% NU = SAMPLE_NU(N, S_22, LAMBDA)

shape = 0.5 * N(:) + 1;
rate  = 0.5 * (S_22(:) + lambda(:));

nu = HIPPO.utils.stats.randgamma(shape, 1.0 ./ rate);
end%sample_nu