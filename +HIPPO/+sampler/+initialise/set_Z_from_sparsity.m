function Z = set_Z_from_sparsity(edges, a)
%SET_Z_FROM_SPARSITY set graph using a certain sparsity level
%
% Z = SET_Z_FROM_SPARSITY(EDGES,A) sets Z s.t. EDGES thresholded with Z has
%    sparsity level A.
%
% This function rounds to the nearest achievable sparsity level.
% Sparsities of 0 and 1 are possible
% Sparsity is 1 - fill level: sparsity of 0 means all edges are in.

assert(a<=1 && a>=0, ... 
	   [mfilename ':set_Z_from_sparsity:BadSparsityParam'], ...
	   'a should be between 0 and 1. \n');
[y,i]                        = sort(edges);
cutOff                       = round((a) * length(edges))+1;
y(1:cutOff-1)                = false;
y(max(1,cutOff):end)         = true;
Z                            = false(size(y));
Z(HIPPO.utils.inverse_permutation(i)) = y;
Z                            = logical(Z);
end%set_Z_from_sparsity