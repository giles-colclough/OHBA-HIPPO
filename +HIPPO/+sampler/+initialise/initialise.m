function [iSave, ...
          initialSparsity, ...
		  Sigma, ...
		  Omega_s, ...
		  mu, ...
		  xi, ...
		  eta, ...
		  geomDistParam, ...
		  edgeSampleWeights, ...
		  samplerParams, ...
		  Z, ...
		  edgeProb, ...
		  sigma_sq, ...
		  sigma_sq_fudge] = initialise(S, p, N, nSubjects, Prior, nWarmup, nEdges)

%INITIALISE HIPPO.SAMPLER.SAMPLE
% Creates vectors to hold the sampled parameters
% 
%  See main sampling file for descriptions of the parameters

global SAMPLING_STAGE


% set save sample number
iSave = 0;

% first random thing we generate is the sparsity level at which we start
% sampling in warmup level 2.
if Prior.Z.do,
	initialSparsity = 0.3 + 0.4 * rand(1);
else
	initialSparsity = 0;
end%if


% compute subject-level covariance estimates
Sigma = bsxfun(@rdivide, S, reshape(N, [1 1 nSubjects]));
% standard unregularised inverse to get precisions
for iS = nSubjects:-1:1,
    Omega_s(:,:,iS) = ROInets.cholinv(Sigma(:,:,iS));
end%for


% set mean edge strength as best guess
mu  = mean(HIPPO.utils.get_edges(Omega_s),2);
xi  = randn(1);
eta = mu./xi;

% inital values for edge inclusions
geomDistParam     = HIPPO.sampler.warmup.set_geom_dist_param(0, nWarmup, Prior);
edgeSampleWeights = HIPPO.sampler.warmup.set_edge_sampling_weights(0, nWarmup, nEdges, Prior);
samplerParams     = HIPPO.sampler.warmup.set_sigma_sq_sampler(0, nWarmup, [], p, [], []);

if SAMPLING_STAGE > 1,
	% we're not doing the initial set-up bit, probably because we're not
	% warming up
	if ~isempty(Prior.Z.initialGuess),
		Z = Prior.Z.initialGuess;
	else
		Z    = HIPPO.sampler.initialise.set_Z_from_sparsity(mu, initialSparsity);
	end
	edgeProb = HIPPO.utils.unvectorise_symmetric_matrix(Z, false, false) + eye(p);
else
	% start with all edges in the model
	Z        = true(nEdges, 1);
	edgeProb = repmat(1, p, p);                                            %#ok<RPMT1>
end%if

% draw sigma from a constrained prior
% sigma_sq = 2 * rand(nEdges,1);
sss      = var(Omega_s, [], 3);
sigma_sq = repmat(mean(sss(triu(true(p),1)))/2, nEdges, 1);
sigma_sq_fudge = sigma_sq;