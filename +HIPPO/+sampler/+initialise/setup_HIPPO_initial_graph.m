function [Zstart, lp] = setup_HIPPO_initial_graph(S, N, X, Prior, nSetupSamples, nSetupChains, nSamplesOut)
%SETUP_HIPPO_INITIAL_GRAPH
%
% Run several initial chains to look for plausible graph structures. Sample
% from best chain without replacement as seeds for the true run

if nargin < 4 || ~exist('nSetupSamples', 'var') || isempty(nSetupSamples),
	nSetupSamples = 1500;
end
if nargin < 5 || ~exist('nSetupChains', 'var') || isempty(nSetupChains),
	nSetupChains = 10;
end
if nargin < 6 || ~exist('nSamplesOut', 'var') || isempty(nSamplesOut),
	nSamplesOut = 1;
end

for i = nSetupChains:-1:1.
	PriorModel(i) = Prior;
	PriorModel(i).Z.initialGuess = [];
end

% set one chain running with the full graph as initialiser
PriorModel(1).Z.initialGuess = ones(rows(S));

% set one chain running with the null graph as initialiser
PriorModel(2).Z.initialGuess = zeros(rows(S));


% and one chain as a sparse graph on the concatenated dataset
SS = sum(S,3);
n  = sum(N(:));
PriorParams = struct('a_lambda', 1/3, 'b_lambda', 0, 'overrelaxation', 0, ...
					 'tau', 0.5, 'proposal', struct('p', 0.3));
Sample = struct('regression',  @sample_BMA_linear_regression, ...
				'lambda',      @sample_BMA_lambda, ...
				'hyperparams', @sample_BMA_hyperparams);
[~, ~, Z] = hippo_single_subject(SS, n, pinv(SS./n), SS./n, ones(1, num_edges(rows(SS))), 1, 2*nSetupSamples, Sample, PriorParams, true);
PriorModel(3).Z.initialGuess = mean(squeeze(Z(:,:,round(3*nSetupSamples/4):end)), 2)>0.5;

% set the remainder of chains running with randomly initalised graphs
% - done automatically

parfor iChain = 1:nSetupChains,
	try
	[omega{iChain},  ...
	 ~,  ...
	 ~,       ...
	 Z_save{iChain},        ...
	 ~,        ...
	 ~,   ...
	 ~, ...
	 ~,   ...
	 ~,        ...
	 ~,      ...
	 ~,        ...
	 ~,   ...
	 lp(:,iChain)]  =    HIPPO_sample(S, N, X, nSetupSamples, nSetupSamples, PriorModel(iChain), 0, 10, [], 0);
	catch ME
		warning(ME.message);
		Z_save{iChain} = NaN(num_edges(rows(S)), 1);
% 		lp(:,iChain)   = repmat(-Inf, nSetupSamples);
	end
end%for

if all(isinf(lp)),
	% bug in lp or something. Just use a sample from the third option
	for iStartVal = nSamplesOut:-1:1,
		Zstart(:,iStartVal) = Z_save{3}(:,randsample(nSetupSamples, 1));
	end
	return
end


% look for most likely models
if any(isnan(lp(:))), 
	lp(isnan(lp)) = []; 
	lp = reshape(lp, [], nSetupChains);
end
rankedProb = cumsum(exp(lp(:) - logsumexp(lp(:))));
iSelect    = datasample(1:length(rankedProb), nSamplesOut, 'replace', false, 'weights', rankedProb); % wswor(rankedProb, nSamplesOut); % weighted sample without replacement
[choosek, chooseChain] = ind2sub(size(lp), iSelect);

% allocate seed values
for iStartVal = nSamplesOut:-1:1,
	Zstart(:,iStartVal) = Z_save{chooseChain(iStartVal)}(:,choosek(iStartVal));
end

fprintf('HIPPO setup done. \n');

