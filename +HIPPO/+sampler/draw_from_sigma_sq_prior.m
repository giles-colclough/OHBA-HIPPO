function sigma_sq = draw_from_sigma_sq_prior(sigma_sq, addedVars)
%DRAW_FROM_SIGMA_SQ_PRIOR draws from prior for any terms which are being
%  proposed as additions to the model (ie are not currently in the model) 
%  and therefore have the prior as their conditional distribution at this point.
%
% SIGMA_SQ = DRAW_FROM_SIGMA_SQ_PRIOR(SIGMA_SQ, ADDEDVARS) adds in to the 
%   existing sigma_sq variable by drawing just from the prior. Where to add is indicated 
%   by the ADDEDVARS vector. 



% Note that the transitions on edges may be quite sensitive to the prior
% which is employed here. 

% This prior is effectively hard-coded into the model, and its parametrisation appears 
% in the MH sampler on sigma_sq, and in the log probability function in SAMPLE, which is not too helpful. 


if any(addedVars),
	sigma_sq(addedVars) = (exp(-log(2) + randn([sum(addedVars), 1]))).^2; % normal prior on log(sigma)
end%if






% A few other priors were experimented with, but were not as successful:

% sigma_sq(addedVars) = GC_randgamma(repmat(2, sum(addedVars), 1), repmat(0.25, sum(addedVars), 1)); % Prior.Sigma.shape, 1.0./Prior.Sigma.rate); 
% sigma_sq(addedVars) = 0.5*rand([sum(addedVars), 1]); % Uniform on (0,0.5). %%exp(5*rand([sum(addedVars), 1])); % a little more constrained than the uninformative prior, but still...
