function xi = sample_xi(sumOmegaS, Z, eta, sigma_sq, nSubjects)
%SAMPLE_XI draw scaling parameter which creates Cauchy distribtion
%
% XI = SAMPLE_XI(SUM_S_OMEGA_S, Z, ETA, SIGMA_SQ, NSUBJETS)

global DEBUG

% catch any eta -> 0 cases. Shouldn't really be here
if any(eta(Z) == 0),
    Z = Z & logical(eta);
    if ~DEBUG,
        warning([mfilename ':EtaZeros'], ...
                ['%s: Some edges have strength 0 but are included in the model. \n', ...
                 '    This is likely a bug. Please investigate. \n'],                ...
                 mfilename);
    end%if
end%if

if all(~Z), % no edges
    mu                  = 0;
    sumEtaSqOverSigmaSq = 0;
else
    sumEtaSqOverSigmaSq = sum(eta(Z).^2 ./ sigma_sq(Z));

    mu = sum(sumOmegaS(Z) .* eta(Z) ./ sigma_sq(Z)) ./ ...
         (1 + nSubjects * sumEtaSqOverSigmaSq);
end%if 
% if eta hits zero, or no terms in model we have a divide-by-zero issue
% in that case, just sample from prior.
if isnan(mu), mu = 0; end%if
 
s  = 1.0 ./ sqrt(1 + nSubjects * sumEtaSqOverSigmaSq);

% xi is normally distributed
xi = mu + s .* randn(1);
end%sample_xi