function [Sampler,      ...
	      Model,        ...
	      nWarmup,      ...
		  Omega_s,      ...
		  Sigma_s,      ...
		  mu,           ...
		  Z,            ...
		  p,            ...
		  chi_sq,       ...
		  sigma_sq,     ...
		  lambda,       ...
		  a,            ...
		  accept_Z,     ...
		  accept_sigma, ...
		  lp]              = read_all_samples(sampleFiles)
%READ_ALL_SAMPLES reads in data from sample chains
%
% Note: check that the files are small enough that all data can be held in
% memory
%	[ ] = READ_ALL_SAMPLES(SAMPLEFILES) 


%	Copyright 2016 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 01-Jun-2016 11:10:52

% if only one file passed in, convert to cell array
if ischar(sampleFiles), sampleFiles = {sampleFiles}; end

nChains = length(sampleFiles);

% check file exists
for iChain = nChains:-1:1,
	if ~exist(sampleFiles{iChain}, 'file'),
		error([mfilename ':FileNotFound'],  ...
			  'File %s does not exist. \n', ...
			  sampleFiles{iChain});
	end%if
end%for

% check memory needed
for iChain = nChains:-1:1,
	f            = dir(sampleFiles{iChain});
	sizechain(iChain) = f.bytes;
end%for

totalGB = sum(sizechain) ./ 1024^3;
if totalGB > 15,                                                           %#ok<BDSCI>
	warning([mfilename ':LargeSampleSize'],                             ...
		    [mfilename ': %d GB RAM required to load all samples. \n'], ...
			totalGB);
		
	reply = input('Do you want to proceed with load? Y/N [Y]:', 's');
	if isempty(reply),
		reply = 'Y';
	end%if
	if ~strcmpi(reply, 'y'),
		return
	end%if
end%if


% read in one chain at a time
for iChain = nChains:-1:1,
	[nSamples(iChain),         ...
	 nWarmup(iChain),          ...
	 thin(iChain),             ...
	 saveWarmup(iChain),       ...
	 randomSeed(iChain),       ...
	 nSubjects(iChain),        ...
	 nModes(iChain),           ...
	 nEdges(iChain),           ...
	 Omega_s(:,:,:,iChain),    ...
	 Sigma_s(:,:,:,iChain),    ...
	 mu(:,:,iChain),           ...
	 Z(:,:,iChain),            ...
	 p(:,:,iChain),            ...
	 chi_sq(:,iChain),         ...
	 sigma_sq(:,:,iChain),     ...
	 lambda(:,:,iChain),       ...
	 a(:,iChain),              ...
	 accept_Z(:,iChain),       ...
	 accept_sigma(:,:,iChain), ...
	 lp(:,iChain)]                = read_single_chain(sampleFiles{iChain});
end%for

% check that we've had consistent dimensions in each chain
checkFun = @(v) assert(all(v(:) == unique(v)),               ...
	                   [mfilename ':DifferentChainOrigins'], ...
					   'Chains seem to have been made by different sample runs. \n');
% checkFun(nSamples);
% checkFun(nWarmup);
% checkFun(thin);
% checkFun(saveWarmup);
checkFun(nSubjects);
checkFun(nModes);
checkFun(nEdges);

% package up summary parts
Model.nSubjects = nSubjects(1);
Model.nModes    = nModes(1);
Model.nEdges    = nEdges(1);

Sampler.nSamples   = nSamples(1);
Sampler.nWarmup    = nWarmup(1);
Sampler.saveWarmup = saveWarmup(1);
Sampler.thin       = thin(1);
Sampler.randomSeed = randomSeed;
Sampler.nChains    = nChains;

end%read_all_samples

function [nSamples, nWarmup, thin,   ...
	      saveWarmup, randomSeed,    ...
	      nSubjects, nModes, nEdges, ...
		  Omega_s,                   ...
	      Sigma_s,                   ...
		  mu,                        ...
		  Z,                         ...
		  p,                         ...
		  chi_sq,                    ...
		  sigma_sq,                  ...
		  lambda,                    ...
		  a,                         ...
		  accept_Z,                  ...
		  accept_sigma,              ...
		  lp]                           = read_single_chain(fileName)
 
[nSamples, nWarmup, thin, saveWarmup, randomSeed, nSubjects, nModes, nEdges, nSaveSamples] = HIPPO.fileio.read_sample_file_header(fileName);
	  
% open file and create automatic destructor
fid     = fopen(fileName, 'r');
cleanMe = onCleanup(@() fclose(fid));

% declare memory
Omega_s       = NaN(nEdges+nModes, nSubjects, nSaveSamples); % single subject precision
Sigma_s       = NaN(nEdges+nModes, nSubjects, nSaveSamples); % single subject covariance
mu            = NaN(nEdges, nSaveSamples); % group mean edge strength
Z             = NaN(nEdges, nSaveSamples); % adjacency matrix
p             = NaN(nEdges, nSaveSamples);       % Edge inclusion probabilities (p(Z=1))
chi_sq        = NaN(nSaveSamples, 1);      % Prior scale on effect size for mu over all edges
sigma_sq      = NaN(nEdges, nSaveSamples); % s.d. of edge strength for each edge over subjects
lambda        = NaN(nSubjects, nSaveSamples); % exponential parameter for prior on all diagonal terms for each subject
a             = NaN(nSaveSamples, 1);         % sparsity
accept_Z      = NaN(nSaveSamples, 1);          
accept_sigma  = NaN(nModes, nSaveSamples);
lp            = NaN(nSaveSamples, 1);            % log probability

% read starting at line 19
HIPPO.fileio.ignore_lines(fid, 18);
for iSample = 1:nSaveSamples,
	if ~mod(iSample, 1000), 
		[~, nameStr] = fileparts(fileName);
		fprintf('Processed %d samples out of %d from file %s. \n', iSample, nSaveSamples, nameStr);
	end%if
	tline                        = fgetl(fid);
	if isempty(tline) || ~ischar(tline), % end of file!
		warning([mfilename ':EarlyBreak'], 'Broke early after processing sample %d\n', iSample);
		break
	end
	try
		[Omega_s(:,:,iSample),    ...
		 Sigma_s(:,:,iSample),    ...
		 mu(:, iSample),          ...
		 Z(:, iSample),           ...
		 p(:,iSample),            ...
		 chi_sq(iSample),         ...
		 sigma_sq(:,iSample),     ...
		 lambda(:,iSample),       ...
		 a(iSample),              ...
		 accept_Z(iSample),       ...
		 accept_sigma(:,iSample), ...
		 lp(iSample)]                 = parse_line_of_samples(tline, nSubjects, nModes, nEdges);
	catch ME
		if strcmpi(ME.message, 'Index exceeds matrix dimensions.'),
			% we've not generated all samples yet - premature analysis!
			warning([mfilename ':Unfinished'], 'Sampling finished early at sample %d\n', iSample-1);
			break
		else
			rethrow(ME);
		end%if
	end%try
end%for
end%read_single_chain

function [Omega_s,      ...
	      Sigma_s,      ...
		  mu,           ...
		  Z,            ...
		  p,            ...
		  chi_sq,       ...
		  sigma_sq,     ...
		  lambda,       ...
		  a,            ...
		  accept_Z,     ...
		  accept_sigma, ...
		  lp]                = parse_line_of_samples(tline, nSubjects, nModes, nEdges)
	  
% read all data from the line into a vector
A = sscanf(tline, '%g,');

% iSample = A(1);
endInd    = 1;

% Omega_s
dataChunk          = (nModes + nEdges)*nSubjects;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
Omega_s            = reshape(A(startInd:endInd), nModes+nEdges, nSubjects);

% Sigma_s
dataChunk          = (nModes + nEdges)*nSubjects;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
Sigma_s            = reshape(A(startInd:endInd), nModes+nEdges, nSubjects);

% mu
dataChunk          = nEdges;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
mu                 = A(startInd:endInd);

% Z
dataChunk          = nEdges;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
Z                  = A(startInd:endInd);

% p
dataChunk          = nEdges;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
p                  = A(startInd:endInd);

% chi_sq
dataChunk          = 1;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
chi_sq             = A(startInd:endInd);

% sigma_sq
dataChunk          = nEdges;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
sigma_sq           = A(startInd:endInd);

% lambda
dataChunk          = nSubjects;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
lambda             = A(startInd:endInd);

% a
dataChunk          = 1;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
a                  = A(startInd:endInd);

% accept_Z
dataChunk          = 1;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
accept_Z           = A(startInd:endInd);

% accept_sigma
dataChunk          = nModes;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
accept_sigma       = A(startInd:endInd);

% lp
dataChunk          = 1;
[startInd, endInd] = move_chunk_on(endInd, dataChunk);
lp                 = A(startInd:endInd);

% check we've got as far as we should
assert(endInd == length(A),          ...
	   [mfilename ':badReadLength'], ...
	   'Not read correct amount of data from sample line. \n');

end%parse_line_of_samples

function [startInd, endInd] = move_chunk_on(endInd, dataChunk)
startInd  = endInd + 1;
endInd    = startInd + dataChunk - 1;
end
% [EOF]