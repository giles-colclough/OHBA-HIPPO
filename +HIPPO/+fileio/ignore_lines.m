function [] = ignore_lines(fid, N)
% move on N lines in the file, without storing contents
for iLine = 1:N,
	fgetl(fid);
end%for
end%ignore_lines