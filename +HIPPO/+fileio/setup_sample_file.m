function fid = setup_sample_file(fileName, nChains, callingFn, nSubjects, p, nEdges, nWarmup, nSamples, saveWarmup, thin, rndSeed)
%SETUP_SAMPLE_FILE  Initialises the file for saved samples
%
%	FILEID = SETUP_SAMPLE_FILE(NAME, NCHAINS, CALLINGFN, NSUBJECTS, P, NEDGES, NWARMUP, NSAMPLES, SAVEWARMUP, THIN, RNDSEED) 
%	
% creates NCHAINS files to save out samples. Each file will be named
% NAME_1.txt, NAME_2.txt, ...
%
% The function returns the vector of file ids for writing, FILEID.
% 
% The remaining inputs define the size of the model and saved paramters
% 
%   CALLINGFN - name of calling function
%   NSUBJECTS
%   P - number of nodes
%   NEDGES - number of edges
%   NWARMUP - number of warmup (burnin) samples
%   NSAMPLES
%   SAVEWARMUP - logical to indicate whether warmup samples are saved
%   THIN       - Thinning factor on sampels
%   RNDSEED    - the seed used to initialise the sampler


%	Copyright 2016 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 01-Jun-2016 11:10:29

[saveDir, nameStem] = fileparts(fileName);
if isempty(saveDir), saveDir = pwd; end

% make sample file names if multiple chains
suffix = '.txt';
if nChains > 1,
	for iChain = nChains:-1:1,
		sampleFile{iChain} = fullfile(saveDir, sprintf('%s_%i%s', nameStem, iChain, suffix));
	end
else
	sampleFile{1} = fullfile(saveDir, [nameStem suffix]);
end

ROInets.make_directory(saveDir);

% if only one random seed is provided, replicate it over all chains
if 1 == length(rndSeed), rndSeed = repmat(rndSeed, nChains, 1); end

for iChain = nChains:-1:1,
	fid(iChain) = create_sample_file(sampleFile{iChain}, iChain, nChains, callingFn, nSubjects, p, nEdges, nWarmup, nSamples, saveWarmup, thin, rndSeed(iChain));
end%for

end%setup_sample_file

function fid = create_sample_file(name, iChain, nChains, callingFn, nSubjects, p, nEdges, nWarmup, nSamples, saveWarmup, thin, rndSeed)

% create file
[fid, MSG] = fopen(name, 'W'); % open for writing without automatic flushing

% check for errors
if -1 == fid, 
	error([mfilename ':FileOpenFailed'],                                     ...
		  'Failed to open file %s for writing with system message:\n\t%s\n', ...
		  name, MSG);
else
	% make cleanup easy
end%if

% write the header
initialise_sample_file(fid, iChain, nChains, name, callingFn, nSubjects, p, nEdges, nWarmup, nSamples, saveWarmup, thin, rndSeed);

end%create_sample_file

function initialise_sample_file(fid, iChain, nChains, name, callingFn, nSubjects, p, nEdges, nWarmup, nSamples, saveWarmup, THIN, rndSeed)


fprintf(fid, '%s\n', name);
fprintf(fid, 'Written on: %s\n', datestr(now, 0));
fprintf(fid, 'By function: %s\n', callingFn);
fprintf(fid, 'Holds samples for chain %i of %i\n', iChain, nChains);
fprintf(fid, '\n\n');
fprintf(fid, 'HEADER\n');
fprintf(fid, 'Model dimensions\n');
fprintf(fid, 'nSubjects,nModes,nEdges\n');
fprintf(fid, '%i,%i,%i\n', nSubjects, p, nEdges);
fprintf(fid, '\n');
fprintf(fid, 'Sampler parameters\n');
fprintf(fid, 'nWarmup,nSamples,thin,saveWarmup,randomSeed\n');
fprintf(fid, '%i,%i,%i,%i,%i\n', nWarmup, nSamples, THIN, saveWarmup, rndSeed);
fprintf(fid, '\n\n');
fprintf(fid, 'SAMPLES\n');
write_variable_names_to_header(fid, nSubjects, p, nEdges);
end%initialise_sample_file

function write_variable_names_to_header(fid, nSubjects, p, nEdges)

% The format of the saved samples will be the following:
% iSample, Omega[iE+ip, iS], Sigma[iE+ip, iS], mu[iE], Z[iE], p[iE], chi_sq, sigma_sq[iE], lambda[iS], a, Lsq[ip, iS], T[ip, iS], nAccept_Z, accept_sigma[ip], accept_t, lp
%
% where iE runs over edges, ip over modes and iS over subjects. 
% Matrices are written one column at a time. 

fprintf(fid, 'iSample,');
for iE = 1:nEdges+p,
	for iS = 1:nSubjects,
		fprintf(fid, 'Omega[%i %i],', iE, iS);
	end
end
for iE = 1:nEdges+p,
	for iS = 1:nSubjects,
		fprintf(fid, 'Sigma[%i %i],', iE, iS);
	end
end
for iE = 1:nEdges,
	fprintf(fid, 'mu[%i],', iE);
end
for iE = 1:nEdges, 
	fprintf(fid, 'Z[%i],', iE);
end
for iE = 1:nEdges, 
	fprintf(fid, 'p[%i],', iE);
end
fprintf(fid, 'chi_sq,');
for iE = 1:nEdges, 
	fprintf(fid, 'sigma_sq[%i],', iE);
end
for iS = 1:nSubjects,
	fprintf(fid, 'lambda[%i],', iS);
end
fprintf(fid, 'a,');
fprintf(fid, 'nAccept_Z,');
for ip = 1:p,
	fprintf(fid, 'accept_sigma[%i],', ip);
end
fprintf(fid, 'lp');
fprintf(fid, '\n');
end%write_variable_names_to_header
% [EOF]