function [ ] = save_samples(saveFile,                                               ...
						    iSave,                                                  ...
							Omega_s,                                                ...
							Sigma_s,                                                ...
							mu,                                                     ...
							Z,                                                      ...
							p,                                                      ...
							chi_sq,                                                 ...
							sigma_sq,                                               ...
							lambda,                                                 ...
							a,                                                      ...
							accept,                                                 ...
							acceptSigma,                                            ...
							lp)
%SAVE_SAMPLES  Saves out current set of samples to file
%	[ ] = SAVE_SAMPLES(FID, iSAVE, OMEGA_S, SIGMA_S, MU, Z, P, CHI_SQ, SIGMA_SQ, LAMBDA, A, ACCEPT, ACCEPTSIGMA, LP)
% 
%   saves out the sampled variables into file held open in FID
% 
%   See HIPPO.SAMPLE.SAMPLER for description of all the parameters


%	Copyright 2016 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 01-Jun-2016 11:07:00

fprintf(saveFile, '%d,',     iSave);
fprintf(saveFile, '%0.18g,', Omega_s); % print down columns. Use 18 sig figs. Separate with comma.
fprintf(saveFile, '%0.18g,', Sigma_s);
fprintf(saveFile, '%0.18g,', mu);
fprintf(saveFile, '%0.18g,', Z);
fprintf(saveFile, '%0.18g,', p);
fprintf(saveFile, '%0.18g,', chi_sq);
fprintf(saveFile, '%0.18g,', sigma_sq);
fprintf(saveFile, '%0.18g,', lambda);
fprintf(saveFile, '%0.18g,', a);
fprintf(saveFile, '%0.18g,', accept);
fprintf(saveFile, '%0.18g,', acceptSigma);
fprintf(saveFile, '%0.18g',  lp);
fprintf(saveFile, '\n');

end%save_samples
% [EOF]