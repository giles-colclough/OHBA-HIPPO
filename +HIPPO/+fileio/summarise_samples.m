function [ ] = summarise_samples(sampleFiles)
%SUMMARISE_SAMPLES produce confidence intervals for each parameter
% 
%  Unfortunately, quite a clunky interface to the problem. 
%  It's easier to read all samples into memory and process there. 
%  You can thin the samples down until you can hold the model in 
%  memory, or come up with a better approach. 

%	Copyright 2016 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 01-Jun-2016 11:10:58

% if only one file passed in, convert to cell array
if ischar(sampleFiles), sampleFiles = {sampleFiles}; end

nChains = length(sampleFiles);

% check file exists
for iChain = nChains:-1:1,
	if ~exist(sampleFiles{iChain}, 'file'),
		error([mfilename ':FileNotFound'],  ...
			  'File %s does not exist. \n', ...
			  sampleFiles{iChain});
	end%if
end%for

% find out how many variables there are (excluding sampleID)
[nSamples, nWarmup, thin, saveWarmup, randomSeed, nSubjects, nModes, nEdges, nSaveSamples] = HIPPO.fileio.read_sample_file_header(sampleFiles{1});
nSampledVars = (nEdges + nModes) * nSubjects * 2 + nEdges * 4 + nSubjects + nModes * nSubjects * 2 + nModes + 5; 

% draw out a set of stats. Held as a vector for easier slicing later.
% 1. Mean
% 2. Std
% 3. Std Error on Mean
% 4. 2.5%
% 5. 5%
% 6. 25%
% 7. 50%
% 8. 75%
% 9. 95%
% 10. 97.5% Confidence Intervals
% 11. number of effective samples
% 12. Rhat

for iVar = nSampledVars:-1:1,
	VarStats(:,iVar) = summarise_var(sampleFiles, iVar, nWarmup, nSamples, saveWarmup);
end%for
end%summarise_samples

function stats = summarise_var(sampleFiles, iVar, nWarmup, nSamples, saveWarmup)
%SUMMARISE_VAR summarise a single variable
% draw out a set of stats. Held as a vector for easier slicing later.
% 1. Mean
% 2. Std
% 3. Std Error on Mean
% 4. 2.5%
% 5. 5%
% 6. 25%
% 7. 50%
% 8. 75%
% 9. 95%
% 10. 97.5% Confidence Intervals
% 11. number of effective samples
% 12. Rhat

stats = zeros(12,1);

if saveWarmup,
	ignoreWarmup = true;
else
	ignoreWarmup = false;
	nWarmup      = 0;
end%if

vals     = read_var_from_chains(sampleFiles, iVar, nWarmup, nSamples, ignoreWarmup);

assert(rows(vals) == nSamples);
nChains = cols(vals);

% basics
stats(1) = mean(vals(:));
stats(2) = std(vals(:));
stats(3) = stats(2) ./ sqrt(nSamples * nChains); % std error

% quantiles
stats(4:10) = prctile(vals(:), [2.5; 5; 25; 50; 75; 95; 97.5]);

% estimated sample size - only derived for normal variates
%Gelman-Rubin statistic - only derived for normal variates
[R, NEFF] = psrf(reshape(vals, [nSamples, 1, nChains]));
stats(11) = sum(NEFF);
stats(12) = R;
end%summarise_var

function vals = read_var_from_chains(sampleFiles, iVar, nWarmup, nSamples, ignoreWarmup)
%READ_VAR_FROM_CHAINS read in a single variable from all chains

nChains = size(sampleFiles);
for iChain = nChains:-1:1,
	vals(:,iChain) = read_var(sampleFiles{iChain}, iVar, nWarmup, nSamples, ignoreWarmup);
end%for
end%read_var_from_chains

function vals = read_var(file, iVar, nWarmup, nSamples, ignoreWarmup)
%READ_VAR read in a single variable from a sample file

% open cleanly
fid     = fopen(file, 'r');
cleanMe = onCleanup(@() fclose(fid));

% read from line 19
nHeader = 18;
if ignoreWarmup, 
	nIgnore = nHeader + nWarmup;
	nRead   = nSamples;
else
	nIgnore = nHeader;
	nRead   = nWarmup + nSamples;
end%if
HIPPO.fileio.ignore_lines(fid, nIgnore);

formatString = [repmat('%*g,', 1, iVar) '%g']; % include the sample number in ignored data

vals = zeros(nRead, 1);
for iSample = 1:nRead,
	tline         = fgetl(fid);
	vals(iSample) = sscanf(tline, formatString, 1);
end%for

end%read_var
% [EOF]