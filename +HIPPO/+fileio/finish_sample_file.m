function [] = finish_sample_file(fid, WARMUP_TIME, SAMPLING_TIME)
%FINISH_SAMPLE_FILE  closes sample file and rounds off. 


%	Copyright 2016 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 01-Jun-2016 14:37:57


fprintf(fid, '\n\n');
fprintf(fid, 'Sampling Complete at %s\n', datestr(now));
fprintf(fid, 'Time for warmup: %d seconds\n', WARMUP_TIME);
fprintf(fid, 'Time for sampling: %d seconds\n', SAMPLING_TIME);

fclose(fid);

end%finish_sample_file
% [EOF]