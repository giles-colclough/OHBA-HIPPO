function [nSamples, nWarmup, thin, saveWarmup, randomSeed, nSubjects, nModes, nEdges, nSaveSamples] = read_sample_file_header(fileName)

% open file and create automatic destructor
fid     = fopen(fileName, 'r');
cleanMe = onCleanup(@() fclose(fid));

% read model dimensions, which start on line 10
HIPPO.fileio.ignore_lines(fid, 9);
tline     = fgetl(fid);
dims      = sscanf(tline, '%d,');
nSubjects = dims(1);
nModes    = dims(2);
nEdges    = dims(3);

% read sampler parameters which starts on line 14
HIPPO.fileio.ignore_lines(fid, 3);
tline      = fgetl(fid);
params     = sscanf(tline, '%f,');
nWarmup    = params(1);
nSamples   = params(2);
thin       = params(3);
saveWarmup = params(4);
randomSeed = params(5);

if saveWarmup,
	nSaveSamples = nWarmup + nSamples;
else
	nSaveSamples = nSamples;
end%if

if thin > 1,
	nSaveSamples = floor(nSaveSamples ./ thin);
end

end%read_sample_file_header