function [] = display_result(DataSet, meanPCorr, PCorrEst)
%DISPLAY_RESULT show the estimates from the demo dataset
% 
% [] = DISPLAY_RESULT(DATASET, MEANSAMPLES, PARTIALCORRELATIONESTIMATE)
% 
%  Compares the mean estimate and 2 subjects to the truth, and to a simple
%  graphical-lasso regularised estimate

% compute glasso estimate
for iSubject = DataSet.nSubjects:-1:1,
	trueP(:,:,iSubject)     = ROInets.convert_precision_to_pcorr(DataSet.P(:,:,iSubject));
	glassoEst(:,:,iSubject) = ROInets.convert_precision_to_pcorr(ROInets.glasso_frequentist(DataSet.Snorm(:,:,iSubject), 0.01));
end%for

CLIM = [-0.6 0.6];

% let's have a look at the mean estimate first
figure('Color', 'w', 'Name', 'Inferred netmats')
subplot(3,3,1)
imagesc(mean(trueP,3)) % mean truth
axis square
axis off
caxis(CLIM)
colormap(bluewhitered)
title('Mean netmat')
ylabel('Truth')

subplot(3,3,4)
imagesc(mean(meanPCorr,3)) % mean estimate
axis square
axis off
caxis(CLIM)
colormap(bluewhitered)
ylabel('HIPPO estimate')

subplot(3,3,7)
imagesc(mean(glassoEst,3)); % glasso mean estimate
axis square
axis off
caxis(CLIM)
colormap(bluewhitered)
ylabel('GLASSO estimate')


for i=2:3,
	subplot(3,3,i)
	imagesc(trueP(:,:,i))
	axis square
	axis off
	caxis(CLIM)
	colormap(bluewhitered)
	title(sprintf('Subject %d netmat', i));
	
	subplot(3,3,i+3)
	imagesc(PCorrEst(:,:,i))
	axis square
	axis off
	caxis(CLIM)
	colormap(bluewhitered)
	
	subplot(3,3,i+6)
	imagesc(glassoEst(:,:,i))
	axis square
	axis off
	caxis(CLIM)
	colormap(bluewhitered)
end%for
	
	
	
	
	
	
	
	
	
	
	
	