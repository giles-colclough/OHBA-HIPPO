function [] = run_demo()
% RUN_DEMO for the HIPPO package
% 
% [] = RUN_DEMO()
% 
% A file designed to give an overview of the interface to the model,
% sampler and sample processing functions. 

saveDir        = '/Users/gilesc/data/HIPPO/gitlab-test-data';
corticalNetDir = saveDir;
netsimDir      = '/Users/gilesc/data/netsims';

%% Create a large set of test data
dataFiles = HIPPO.demo.create_test_data(saveDir, corticalNetDir, netsimDir);
nDataSets = length(dataFiles);

%% Settings for the model
useSparsePrior = false;
Prior          = HIPPO.prior.default_setup(useSparsePrior);

%% Running a single chain on one model
pickModel  = 7;
load(dataFiles{pickModel});
nWarmup    = 5000;
nSamples   = 5000;
saveWarmup = false;
thin       = 5;
saveFileName = [];
% on a real dataset, you would use HIPPO.ROIdata.estimate_temporal_samples to estimate the appropriate number of independent data samples. 
% See HIPPO.demo.create_test_data for an example.
N       = repmat(DataSet.nSamples, DataSet.nSubjects, 1); 

% sample precisions and covariance matrices for each subject
[Omega_s, Sigma_s] = HIPPO.sampler.sample(DataSet.Snorm, N, nWarmup, nSamples, Prior, saveWarmup, thin, saveFileName);

% Re-base precisions to partial correlations
[partialCorrelation, correlation, meanPCorr] = HIPPO.utils.normalise_samples(Omega_s, Sigma_s);
PCorrEst = squeeze(mean(partialCorrelation,4));
CorrEst  = squeeze(mean(correlation,4));

%% Visualise, infer or do other things here.
% I wouldn't run the below for any model with greater than 8 nodes.
HIPPO.plot.plot_covariance_samples(meanPCorr, PCorrEst, partialCorrelation);
HIPPO.demo.display_result(DataSet, meanPCorr, PCorrEst);

%% Run again, saving samples to a file, and using the sparse model
useSparsePrior = true;
Prior          = HIPPO.prior.default_setup(useSparsePrior);
saveToDir      = fullfile(saveDir, ['results-' num2str(pickModel)]);
saveFileName   = fullfile(saveToDir, 'my_samples_1.txt');
nWarmup        = 10000;
nSamples       = 30000;
thin           = 30;
HIPPO.sampler.sample(DataSet.Snorm, N, nWarmup, nSamples, Prior, saveWarmup, thin, saveFileName);

[Sampler,      ...
 Model,        ...
 nWarmup,      ...
 Omega_s,      ...
 Sigma_s,      ...
 mu,           ...
 Z,            ...
 p,            ...
 chi_sq,       ...
 sigma_sq,     ...
 lambda,       ...
 a,            ...
 ~,            ...
 ~,            ...
 lp]              = HIPPO.fileio.read_all_samples(saveFileName);

[partialCorrelation, correlation, meanPCorr] = HIPPO.utils.normalise_samples(Omega_s, Sigma_s);
PCorrEst = squeeze(mean(partialCorrelation,4));
CorrEst  = squeeze(mean(correlation,4));

%% And a code snippet that is more suitable for scripting, or running on a cluster
%{
for iDataSet = nDataSets:-1:1,
	subSaveDir = fullfile(saveDir, ['results-' num2str(iDataSet)]);
	ROInets.make_directory(subSaveDir);
	load(dataFiles{iDataSet});
	Snonorm = DataSet.S;
	Snorm   = DataSet.Snorm;
	N       = repmat(DataSet.nSamples, DataSet.nSubjects, 1);
	S       = Snorm;
	dataStore  = fullfile(subSaveDir, 'data.mat');
	priorStore = fullfile(subSaveDir, 'prior_noZ.mat');
	save(dataStore, 'S', 'N', 'Snonorm');
	save(priorStore, 'Prior');
	exitOnFinish = false;
	HIPPO.parallel.run_chain(dataStore, priorStore, nWarmup, nSamples, saveWarmup, thin, sampleFile, exitOnFinish) % this line could be called from a bash script e.g. matlab -r 'HIPPO.parallel.run_chain(...)'
end%for
%}
