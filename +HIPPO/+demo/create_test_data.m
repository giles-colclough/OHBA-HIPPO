function dataFiles = create_test_data(saveDir, corticalNetDir, netsimDir)
% CREATE_TEST_DATA Generate battery of mvnormal datasets
%
% DATAFILES = CREATE_TEST_DATA(SAVEDIR, CORTICALNETDIR, NETSIMDIR) creates 8 simulations
%   in SAVEDIR, generating normally distributed data under a variety of
%   network models. 
% 
%   For all datasets to work, you need to have Steve Smith's 4th Netsim 
%   in NETSIMDIR (sim4.mat, available from www.fmrib.ox.ac.uk/~steve/netsim/)
%   and the matrices cat.mat and fve30.mat from the Brain Connectivity Toolbox
%   (sites.google.com/site/bctnet/datasets/) in CORTICALNETDIR. 
%
% Each DataSet has the following fields:
%  - Name
%  - nModes
%  - nSamples
%  - nSubjects
%  - P         % ground truth precision from which data are generated
%  - Sigma     % ground truth covariance
%  - S         % sum of squares
%  - Snorm     % sum of squares of variance normalised data

% directory in which to place results
if nargin < 1 || isempty(saveDir),
	saveDir = '/Users/gilesc/data/HIPPO/gitlab-test-data';
end
% directory in which to find cat and macaque cortical network matrices
if nargin < 2 || isempty(corticalNetDir), 
	corticalNetDir = saveDir;
end
% directory in which to find Smith's netsims
if nargin < 3 || isempty(netsimDir),
	netsimDir = '/Users/gilesc/data/netsims';
	if ~isdir(netsimDir), netsimDir = []; end % don't run this one
end
ROInets.make_directory(saveDir);

nDataSets = 10;
dataFiles = arrayfun(@(d) fullfile(saveDir, sprintf('testData%d.mat', d)), 1:nDataSets, 'UniformOutput', false);

iModel = 0;

%% 6-node classic with different parameters
iModel = iModel + 1;
DataSet = [];
DataSet.Name      = 'Wang';
DataSet.nModes    = 6;
DataSet.nSamples  = 18;
DataSet.nSubjects = 5;
p = DataSet.nModes;
P = eye(p); 
for i = 1:(p-1),
    P(i,i+1) = 0.5;
    P(i+1,i) = 0.5;
end%for
P(1,p) = 0.4;
P(p,1) = 0.4;
DataSet.P = repmat(P, [1 1 DataSet.nSubjects]);
DataSet.Sigma = repmat(HIPPO.utils.cholinv(P), [1 1 DataSet.nSubjects]);
[DataSet.S, DataSet.Snorm] = generate_normal_data(DataSet);

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');


iModel = iModel + 1;
DataSet = [];
DataSet.Name      = 'Wang';
DataSet.nModes    = 6;
DataSet.nSamples  = 18;
DataSet.nSubjects = 25;
p = DataSet.nModes;
P = eye(p); 
for i = 1:(p-1),
    P(i,i+1) = 0.5;
    P(i+1,i) = 0.5;
end%for
P(1,p) = 0.4;
P(p,1) = 0.4;
DataSet.P = repmat(P, [1 1 DataSet.nSubjects]);
DataSet.Sigma = repmat(HIPPO.utils.cholinv(P), [1 1 DataSet.nSubjects]);
[DataSet.S, DataSet.Snorm] = generate_normal_data(DataSet);

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');

iModel = iModel + 1;
DataSet = [];
DataSet.Name      = 'Wang';
DataSet.nModes    = 6;
DataSet.nSamples  = 50;
DataSet.nSubjects = 25;
p = DataSet.nModes;
P = eye(p); 
for i = 1:(p-1),
    P(i,i+1) = 0.5;
    P(i+1,i) = 0.5;
end%for
P(1,p) = 0.4;
P(p,1) = 0.4;
DataSet.P = repmat(P, [1 1 DataSet.nSubjects]);
DataSet.Sigma = repmat(HIPPO.utils.cholinv(P), [1 1 DataSet.nSubjects]);
[DataSet.S, DataSet.Snorm] = generate_normal_data(DataSet);

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');

iModel = iModel + 1;
DataSet = [];
DataSet.Name      = 'Wang';
DataSet.nModes    = 6;
DataSet.nSamples  = 100;
DataSet.nSubjects = 25;
p = DataSet.nModes;
P = eye(p); 
for i = 1:(p-1),
    P(i,i+1) = 0.5;
    P(i+1,i) = 0.5;
end%for
P(1,p) = 0.4;
P(p,1) = 0.4;
DataSet.P = repmat(P, [1 1 DataSet.nSubjects]);
DataSet.Sigma = repmat(HIPPO.utils.cholinv(P), [1 1 DataSet.nSubjects]);
[DataSet.S, DataSet.Snorm] = generate_normal_data(DataSet);

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');

%% Netsim
% use sim 4, as it has 50 nodes
if ~isempty(netsimDir)
	iModel = iModel + 1;
	DataSet = [];
	DataSet.Name      = 'NetSim 4';
	DataSet.nModes    = 50;
	DataSet.nSubjects = 50;
	load(fullfile(netsimDir, 'sim4.mat')); % loads CIJts, Nnodes, Nsubjects, Ntimepoints, net, ts
	Ptmp = squeeze(net(1,:,:));
	A    = logical(Ptmp + Ptmp'); % mask with adjacenecy matrix from net GT
	P    = corrcov(HIPPO.utils.cholinv(cov(ts))) .* A; % use precision matrix computed from all data
	DataSet.P = repmat(P, [1 1 DataSet.nSubjects]);
	DataSet.Sigma = repmat(HIPPO.utils.cholinv(P), [1 1 DataSet.nSubjects]);
	for iS = 1:DataSet.nSubjects,
		dataChunk             = ts((iS-1) * Ntimepoints + 1 : iS * Ntimepoints, :);
		dataCov               = cov(dataChunk, 1);
		DataSet.S(:,:,iS)     = Ntimepoints * dataCov;
		DataSet.Snorm(:,:,iS) = Ntimepoints * corrcov(dataCov, 1);
		nEst(iS)              = HIPPO.ROIdata.estimate_temporal_samples(dataChunk');
	end
	DataSet.nSamples = round(mean(nEst));
end%if

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');

%% Random entries with sparsity p=0.5
iModel = iModel + 1;
DataSet = [];
DataSet.Name      = 'Random switching';
DataSet.nModes    = 25;
DataSet.nSamples  = 500;
DataSet.nSubjects = 25;
nEdges            = HIPPO.utils.num_edges(DataSet.nModes);
tmp               = mnrnd(1, [1/3, 1/3, 1/3], nEdges);
for iCol = 1:3,
	tmp2((tmp(:,iCol) == 1),1) = iCol;
end
probMat           = HIPPO.utils.unvectorise_symmetric_matrix((tmp2 - 1) ./ 2, false, false); % probabilities of 0, 0.5 or 1
% fill in diagonal
probMat(logical(eye(DataSet.nModes))) = 1;
MeanStrengths = 0.25 + 0.05 * randn(nEdges, DataSet.nSubjects);
P = HIPPO.utils.unvectorise_symmetric_matrix(MeanStrengths, false, false) .* repmat(probMat, [1 1 DataSet.nSubjects]);
Pgen = HIPPO.utils.unvectorise_symmetric_matrix(MeanStrengths, false, false) .* repmat(logical(probMat), [1 1 DataSet.nSubjects]);
for iS = 1:DataSet.nSubjects,
	% fill in diagonal
	X = P(:,:,iS);
	Xg = Pgen(:,:,iS);
	X(logical(eye(DataSet.nModes))) = 2;
	Xg(logical(eye(DataSet.nModes))) = 2;
	P(:,:,iS) = corrcov(X);
	Pgen(:,:,iS) = corrcov(Xg);
end

for iS = 1:DataSet.nSubjects,
	for iT = DataSet.nSamples:-1:1,
		A = binornd(1, probMat);
		dat(iT,:) = randnorm(1, zeros(DataSet.nModes, 1), [], pinv(Pgen(:,:,iS).*A));
	end
	DataSet.S(:,:,iS) = dat'*dat;
	DataSet.Snorm(:,:,iS) = corrcov(cov(dat,1),1)*DataSet.nSamples;
	DataSet.P(:,:,iS)     = P(:,:,iS);
	DataSet.Sigma(:,:,iS) = pinv(P(:,:,iS));
end
save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');






%% Variable Wang
iModel = iModel + 1;
DataSet = [];
DataSet.Name      = 'Wang variable';
DataSet.nModes    = 6;
DataSet.nSamples  = 25;
DataSet.nSubjects = 25;
p = DataSet.nModes;
P = eye(p); 
for i = 1:(p-1),
    P(i,i+1) = 0.5;
    P(i+1,i) = 0.5;
end%for
P(1,p) = 0.4;
P(p,1) = 0.4;
[DataSet.P, DataSet.Sigma] = fill_adj_matrix(logical(P), DataSet.nSubjects, 0.4, 0.2);
[DataSet.S, DataSet.Snorm] = generate_normal_data(DataSet);

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');


iModel = iModel + 1;
DataSet = [];
DataSet.Name      = 'Wang big variable';
DataSet.nModes    = 30;
DataSet.nSamples  = 100;
DataSet.nSubjects = 25;
p = DataSet.nModes;
P = eye(p); 
for i = 1:(p-1),
    P(i,i+1) = 0.5;
    P(i+1,i) = 0.5;
end%for
P(1,p) = 0.4;
P(p,1) = 0.4;
[DataSet.P, DataSet.Sigma] = fill_adj_matrix(logical(P), DataSet.nSubjects, 0.4, 0.15);
[DataSet.S, DataSet.Snorm] = generate_normal_data(DataSet);

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');




%% Cat cortex
iModel = iModel + 1;
load(fullfile(saveDir, 'cat.mat')); % loads CIJall and CIJctx
DataSet = [];
DataSet.Name      = 'Cat  cortex';
DataSet.nModes    = 52;
DataSet.nSamples  = 500;
DataSet.nSubjects = 30;
[DataSet.P, DataSet.Sigma] = fill_adj_matrix_Wishart(logical(CIJctx), DataSet.nSubjects, 400);
[DataSet.S, DataSet.Snorm] = generate_normal_data(DataSet);

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');



%% Macaque visual cortex
iModel = iModel + 1;
load(fullfile(saveDir, 'fve30.mat')); % loads CIJ
DataSet = [];
DataSet.Name      = 'Macaque visual cortex';
DataSet.nModes    = 30;
DataSet.nSamples  = 100;
DataSet.nSubjects = 25;
[DataSet.P, DataSet.Sigma] = fill_adj_matrix_Wishart(CIJ, DataSet.nSubjects, 50);
[DataSet.S, DataSet.Snorm] = generate_normal_data(DataSet);

save(fullfile(saveDir, ['testData' num2str(iModel)]), 'DataSet');














function [S, Snorm] = generate_normal_data(DataSet)
% simulates from a precision matrix
n = DataSet.nSubjects;
for iS = n:-1:1,
	tmp           = chol(DataSet.P(:,:,iS)) \ randn(DataSet.nModes, DataSet.nSamples);
    S(:,:,iS)     = tmp * tmp';
	Snorm(:,:,iS) = corrcov(S(:,:,iS)./DataSet.nSamples).* DataSet.nSamples;
end

function [P, Sigma] = fill_adj_matrix(A, nSubjects, m, s)
% creates values on adjacency matrix and returns SPD precision
A(logical(vec(eye(size(A))))) = 0;
edgesA = HIPPO.utils.get_edges(A);
nEdges = length(edgesA);
meanStrengths = 2 * (-0.5 + binornd(1,0.5,nEdges,1)) .* abs(m + 0.2 * randn(nEdges,1));
strengths     = repmat(meanStrengths, 1, nSubjects) + s * randn(nEdges, nSubjects);
edges         = strengths .* repmat(edgesA, 1, nSubjects);
P = HIPPO.utils.unvectorise_symmetric_matrix(edges, false, false) + repmat(eye(size(A)), [1 1 nSubjects]);

for iS = 1:nSubjects,
	K = P(:,:,iS);
	K(logical(vec(eye(size(A))))) = 1;
	K = HIPPO.external.nearestSPD(K) .* (A + eye(size(A)));
	% test that Ahat is in fact PD. if it is not so, then tweak it just a bit.
	p = 1;
	k = 0;
	while p ~= 0
	  [R,p] = chol(K);
	  k = k + 1;
	  if p ~= 0
		% Ahat failed the chol test. It must have been just a hair off,
		% due to floating point trash, so it is simplest now just to
		% tweak by adding a tiny multiple of an identity matrix.
		K = K + 0.1*eye(size(A));
	  end
	end
	assert(ROInets.isposdef(K));
	K = corrcov(K, 1);
	Sigma(:,:,iS) = HIPPO.utils.cholinv(K);
	P(:,:,iS) = K;
end

function [P, Sigma] = fill_adj_matrix_Wishart(A, nSubjects, df)
% creates values on adjacency matrix and returns SPD precision using a
% G-Wishart generative model

% we're not dealing with symmetric matrices here, so let's just use the
% upper triangular part and be done with it.
A(tril(true(size(A)),-1)) = 0;
A = A + A';
A(logical(vec(eye(size(A))))) = 1;
edgesA = HIPPO.utils.get_edges(A);
nModes = rows(A);

T = corrcov(HIPPO.utils.stats.sample_G_Wishart(eye(nModes), nModes - 1, logical(A), 1) ./ (nModes - 1), 1);
while ~isposdef(T),
	T = corrcov(HIPPO.utils.stats.sample_G_Wishart(eye(nModes), nModes - 1, logical(A), 1) ./ (nModes - 1), 1);
end
meanStrengths = T;

for iS = 1:nSubjects,
	fprintf('Filling in P for subject %d out of %d\n', iS, nSubjects);
	T = corrcov(HIPPO.utils.stats.sample_G_Wishart(meanStrengths, df, logical(A), 1) ./ (df), 1);
	while ~isposdef(T)
	    T = corrcov(HIPPO.utils.stats.sample_G_Wishart(meanStrengths, df, logical(A), 1) ./ (df), 1);
	end
	P(:,:,iS) = T .* A;
	Sigma(:,:,iS) = HIPPO.utils.cholinv(P(:,:,iS));
end%for
