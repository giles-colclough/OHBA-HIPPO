#!/bin/bash
#run_HIPPO.bash
#
# Giles Colclough
# 1 June 2016
#
# Usage: run_HIPPO.bash OUTPUT_DIR NCHAINS NWARMUP NSAMPLES SAVEWARMUP THIN
#  Designed to work with fsl_sub - but I was struggling to make it work, unfortunately.

/usr/local/bin/virtual-x -f > /tmp/$$.virtual-display
disp=`grep 'display started' /tmp/$$.virtual-display | awk -F: '{print $NF}'`
rm /tmp/$$.virtual-display
export DISPLAY=":$disp"

CWD=${PWD}


if [ $BASH_ARGC ]
then
    MODEL_DIR=$1
    NCHAINS=$2
    NWARMUP=$3
    NSAMPLES=$4
    SAVEWARMUP=$5
    THIN=$6
else
    MODEL_DIR="${PWD}"
    NCHAINS=4
    NWARMUP=1000
    NSAMPLES=5000
    SAVEWARMUP=1
    THIN=1
fi

OUTPUT_DIR="${MODEL_DIR}"
SRC_DIR="/Users/gilesc/Documents/IBME-SVN-Repo/src/subject-parcellation/model-I/Hippo_model_1c/"

DATA_FILE="${MODEL_DIR}/data.mat"
PRIOR_FILE="${MODEL_DIR}/prior.mat"


# move into the source directory so matlab can run the code
cd ${SRC_DIR}

for ((i=1; $i-($NCHAINS+1); i=$i+1))
do
    LOG_FILE="${OUTPUT_DIR}/matlab_output_chain_${i}.txt"
    SAMPLE_FILE="${OUTPUT_DIR}/HIPPO_samples_${i}"
	echo "Starting chain $i"
    cmd="matlab -nodisplay -nodesktop -nosplash -logfile ${LOG_FILE} \< HIPPO.parallel.run_chain('${DATA_FILE}', '${PRIOR_FILE}', ${NWARMUP}, ${NSAMPLES}, ${SAVEWARMUP}, ${THIN}, '${SAMPLE_FILE}', 1)"
    echo "${cmd}"
    if [ $i -le ${NCHAINS} ]
    then
        ${cmd} &
    else
        ${cmd}  # Remove ampersand here to not finish program until last sampler complete.
    fi
    echo ''
done

echo ''
echo 'All chains released. '
echo ''

cd ${CWD}
virtual-x -k $disp

# [EOF]