function hh = plot_covariance_samples(M, S, Ssample, nHist)
%PLOT_COVARIANCE_SAMPLES plots posterior of M based on random samples
% 
% PLOT_COVARIANCE_SAMPLES(M) plots the posterior of each marginal element
%   of M, and a represenation of the data which M describes, in a tiled
%   histogram plot. M is square, with samples in the third dimension. 
%
% PLOT_COVARIANCE_SAMPLES(M, S) also plots locations of mean subject-level
%   matrices estimated by the model. If there are 20 subjects, S has 20
%   matrices stacked in the third dimension. These should be the means of
%   the subject-level inferences. 
%
% PLOT_COVARIANCE_SAMPLES(M, S, S_SAMPLED) treats S as point estimates at
%   subject level, and S_SAMPLED as the posterior estimates of S. S_SAMPLED
%   should have samples in the fourth dimension. 
% 
% PLOT_COVARIANCE_SAMPLES(M, S, S_SAMPLED, NHIST) uses NHIST points to 
%   compute the histograms. 
%
% H = PLOT_COVARIANCE_SAMPLES(...) returns the handle H to the figure
%   window.
% 
% EXAMPLE:
% HIPPO.plot.plot_covariance_samples(meanSamples, PCorrEst, PCorr);
%  for samples of the mean partial correlation matrix meanSamples, the
%  estimated subject-level partial correlation matrices PCorrEst, and
%  samples of the partial correlation matrices PCorr (4D). 

%	Copyright 2015 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 21-Aug-2015 11:52:14

%% Input Checking and parameter setup

% basic parameters for plotting
NPLOTPOINTS = 50; % maximum number of scatter points before things get busy
FONTSIZE    = 10;
MARKERSIZE  = 4;

% check matrix input
[check, nModes, nSamples] = size(M);

assert(check == nModes,                                                    ...
       [mfilename ':NotSquare'],                                           ...
       '%s: input matrices should be square in the first two dimensions.', ...
       mfilename);

if nargin >= 2 && ~isempty(S),
    [check2, check3, nSubjects] = size(S);
    
    assert(check2 == nModes && check3 == nModes,                                  ...
           [mfilename ':SizeMismatch'],                                           ...
           '%s: Size of M and S should be the same in the first two dimensions.', ...
           mfilename);
    
    addSubjLevel = true;
    % take a reduced subset of subjects to reduce clutter
    if nSubjects > NPLOTPOINTS,
        S         = S(:,:,randperm(nSubjects, NPLOTPOINTS));
        nS        = NPLOTPOINTS; 
    else 
        nS        = nSubjects;
    end%if
else
    addSubjLevel = false;
end%if

if nargin >= 3 && ~isempty(Ssample),
    [check4, check5, check6, nPostSamples] = size(Ssample);
    assert(check4 == nModes && check5 == nModes && check6 == nSubjects, ...
           [mfilename ':SampleSizeMismatch'],                           ...
           '%s: Size of S_SAMPLED should match first three dimensions of S. \n', ...
           mfilename);
    
    addPointEst = true;
    if nSubjects > NPLOTPOINTS,
        Ssample = Ssample(:,:,randperm(nSubjects, NPLOTPOINTS),:);
    end%if
else
    addPointEst = false;
end%if
   
if nargin < 4 || ~exist('nHist', 'var') || isempty(nHist),
    nHist = max(30, min(fix(nSamples./10) , 100));
end%if

nSubSamples = min(NPLOTPOINTS, nSamples);

%% Figure plotting
% setup display
h = figure('Name', 'samples from spd matrix', 'Color', 'w');

% generate each tile in turn
for i = 1:nModes,
    for j = 1:nModes,
        subplot(nModes, nModes, nModes * (j-1) + i);
        
        % histogram of variance on diagonal, histogram of covariance on
        % lower triangle
        if i <= j,
            % histogram in grey
            [c,x] = hist(squeeze(M(i,j,:)), nHist);
            NORM  = sum(c);
            hbar  = bar(gca, x, c./NORM, 0.8); % normalise counts
            set(hbar, 'FaceColor', [205, 205, 205]/255, ... 
                'EdgeColor', 'none');
            
            % smoother version in blue line
            hold on;
            cc = sgolayfilt(c, 3, 21);
            plot(x, cc./NORM, ...
                 'LineWidth', 1.5, 'Color', [99 184 255]/255);
            
            m = mean(M(i,j,:));
            vline(m, 'r', sprintf('%0.2f', m));
            
            % add in subject-level means?
            if addSubjLevel,
                plot(squeeze(S(i,j,:)),                                        ...
                     repmat(0.05 * max(c) ./ NORM, nS, 1),                      ...
                     '+', 'MarkerSize', MARKERSIZE, 'Color',[255 153 28]./255, ...
                     'LineStyle', 'none');
            end%if 
            
            % basic prettification
            ylim([0 Inf])
            HIPPO.plot.tidyAxes(gca, FONTSIZE);
            set(gca, 'box', 'off', 'YGrid', 'off'); %'YColor', 'none'
            axis square
           
       
        else
            % show posterior of each edge on subject, together with
            % point-estimate subject mean
            if addPointEst,
                HIPPO.plot.plot_mean_shrinkage(squeeze(S(i,j,:)), ...
                                    squeeze(Ssample(i,j,:,:)));
            
            % scatter plot of variables drawn from covariance matrix on upper
            % triangle   
            else
                try
                % draw a load of points from the marginal distribution of two
                % variables
                pickSamples = randperm(nSamples, nSubSamples);
                for iS = nSubSamples:-1:1,
                    y(:,iS) = randnorm(1, zeros(2,1), [], ...
                                      M([i j], [i j], pickSamples(iS)));
                end%for
                plot(y(1,:).', y(2,:).', 'o', 'LineStyle', 'none', 'MarkerSize', MARKERSIZE, ...
                     'Color', [99 184 255]./255);
                HIPPO.plot.tidyAxes(gca, FONTSIZE);
                set(gca, 'box', 'on', 'YGrid', 'off');
                catch ME % sometimes pos def fails in drawing points
                    if strcmpi(ME.identifier, 'MATLAB:posdef'),
                        continue
                    else
                        rethrow(ME);
                    end%if
                end%catch
            end%if
        end%if
    end%for
end%for

% assist saving
set(h, 'PaperPositionMode', 'auto', 'PaperUnits', 'points', ...
    'Position', get(h, 'Position')); % don't ask why this one seems to help

% return handle? - suppress output to screen if no output argument
if nargout, hh = h; end

end%plot_covariance_samples
% [EOF]