function hh = plot_mean_shrinkage(m, s)
%PLOT_MEAN_SHRINKAGE plots sample means and inferred posteriors
%
% PLOT_MEAN_SHRINKAGE(M, S) plots, for each mean in M, the inferred
%   distribution from the samples in the columns of S
% 
% H = PLOT_MEAN_SHRINKAGE(...) returns the handle H to the axis

%	Copyright 2015 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 21-Aug-2015 11:52:14

% plotting parameters
FONTSIZE    = 10;
MARKERSIZE  = 6;

% check inputs
nSubjects = ROInets.rows(m);
[check, nSamples] = size(s);
assert(check == nSubjects,                                        ...
       [mfilename ':Size mismatch'],                              ...
       '%s: the first dimension of m and s must be the same. \n', ...
       mfilename);
   

h = gca;
% plot distributions on each subject
distMean = mean(s, 2);
for iS = 1:nSubjects,
    ss           = s(iS,:);
    distMin(iS)  = prctile(ss(ss~=0), 2.5, 2); % useful when plotting spike and slab distributions
    distMax(iS)  = prctile(ss(ss~=0), 97.5, 2);
    plot([distMin(iS), distMax(iS)], [iS, iS], '-', ...
         'Color', [205, 205, 205]/255, 'LineWidth', 3);
     
    hold(h, 'on')
end%for
plot(h, distMean, (1:nSubjects).', '+', 'MarkerSize', MARKERSIZE, ...
     'Color', [125, 125, 125]/255, 'LineStyle', 'none');
 
 
% plot subject estimates
plot(h, m, (1:nSubjects).', '+', 'MarkerSize', MARKERSIZE, ...
     'Color',[255 153 28]./255, 'LineStyle', 'none');
 
% tidy up
HIPPO.plot.tidyAxes(h, FONTSIZE);
set(h, 'box', 'off', 'YGrid', 'off'); %      'YColor', 'none'
ylim([0 nSubjects+1])
hold(h, 'off');

% return handle? - suppress output to screen if no output argument
if nargout, hh = h; end

end%plot_mean_shrinkage
% [EOF]