function [SSsave] = sample_ROI_SS(X, spatialMap, Prior, includeMean, nSamples, nWarmup)
%SAMPLE_ROI_SS draw sum of squares matrices for a set of ROIs 
%
% This function performs (robust) linear regression on voxel data to 
% identify parcel time-courses. It then summarises the variability in these
% time-courses as a set of samples of the sum-of-squares matrix, to pass up
% to a network inference program.
%
%	[SS] = SAMPLE_ROI_SS() 
%
%
% Prior: .nu - set to inf for normal distribution, otherwise DoF of
%              multivariate t.
%        .sigma.a [0]
%              .b [0]
%        .phi.do
%            .a
%            .
%        .Lambda.do     [0]
%        .Lambda.matrix [0]
%        .Lambda.V
%        .Lambda.v
%
% includeMean is a flag: if TRUE,  then SS = YY';
%                        if FALSE, then SS = (Y-m) (Y-m)';
%   The latter is faster, but may be of less use: in a hierarchical model,
%   the sum-of-squares of Y would have been passed up. 

%	References:
%	
%	

%	Copyright 2015 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 28-Oct-2015 15:14:36


error('This function is nearly ready, but not quite. \n')

%% INput parsing
[nVoxels, nPoints] = size(X);
[checkMe, nModes]  = size(spatialMap);
assert(checkMe == nVoxels,        ...
       [mfilename ':VoxelError'], ...
       'The spatial map and input data should have the same number of voxels. \n');

% If nu is non-infinite, run robust regression
if isinf(Prior.nu),
    isRobust = false;
else
    assert(isscalar(nu) && ~mod(nu,1) && nu >= 3, ...
           [mfilename ':badNu'],                  ...
           'Nu must be a scalar degrees of freedom > 2, or infinite. \n');
    isRobust = true;
end%if

dataSS = X * X';

nSamplesTotal = nSamples + nWarmup;
   
%% initialise parameters
SSsave = NaN(nModes, nModes, nSamples);

%% Run sampling
if isRobust,
    error('Robust regression not currently supported. \n');
    
else
    for iSample = 1:nSamplesTotal,
        % sample elements of T
        if Prior.phi.do,
            error('Independent noise sampling not currently supported. \n');
        else
            T_inv = eye(nVoxels);
        end%if
        
        % sample Lambda0, prior covariance on Y
        if Prior.Lambda.do,
            error('Hyperprior on precision matrix not currently supported. \n');
        else
            Lambda0 = eye(nModes);
        end%if
        
        % sample sigmasq
        scaledPrecision = spatialMap' * T_inv * spatialMap + Lambda0;
        R               = chol(scaledPrecision);
        
        bPost    = 0.5 * (2 * Prior.sigma.b                   ...
                          + sum(diag(dataSS) .* diag(T_inv))  ...
                          - trace(dataSS * T_inv * spatialMap ...
                                  * (R \ (R' \ spatialMap'))));
                              
        sigma_sq = GC_randinvgamma(0.5 * nVoxels * nPoints + Prior.sigma.a, ...
                                   bPost);
        
        
        % sample Y
        mu = R \ (R' \ spatialMap' * T_inv * X);
        Y  = mu + (R ./ sqrt(sigma_sq)) \ randn(nModes, nPoints);
            
        if includeMean,
            % form SS matrix
            SS = Y*Y';
        else
            Ydm = bsxfun(@minus, Y, sum(Y,2)./nPoints); % inline mean
            SS  = Ydm * Ydm';
%             THIS BLOCK IS WRONG: not including mean is about not
%             including mean OVER TIME
%             The mean value mu, calculated here, is time-varying and
%             crucial
%             % form SS matrix directly
%             % sample U'*U from a Wishart with unit scale
%             % See http://en.wikipedia.org/wiki/Wishart_distribution#Theorem
%             U     = randwishart(df, p); % Upper chol of unit scale Wishart
%             Spart = R \ U'; % THIS SEEMS WRONG why is the covariance only determined by the spatial map, not the data?
%             SS    = Spart * Spart' .* sigma_sq;
        end%if
        
        % save result
        if iSample > nWarmup,
            iSave = iSample - nWarmup;
            SSsave(:,:,iSave) = SS;
        end%if
    end%sampling loop
end%if is robust
end%sample_ROI_SS
% [EOF]