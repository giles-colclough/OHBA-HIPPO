function edgeInds = edge_inds(nNodes, iCol)
%EDGE_INDS retrieves the edge indices for a column of a matrix, where edges
%  are the concatenated upper triangular part.
%
% INDS = EDGE_INDS(NNODES, COL) returns the indices for a column of a
%    square symmetric matrix of size NNODES, as found in the concatenated
%    upper triangular part. 
%
% E.G. 
%    p = size(M,1);
%    edges = M(triu(true(p),1));
%    iCol = 3;
%    display(M(ROInets.setdiff_pos_int(1:p,3),3));
%    display(edges(edge_inds(p,iCol)));

% start index is (iCol-2) terms of 1 + 2 + 3 + ..., +1
if 1 == iCol,
    % nodes in first column below diagonal
    ii       = (2:nNodes).';
    edgeInds = (ii - 2) .* (ii - 1) ./2 + 1;
else
    startInd = (iCol-2) .* (iCol - 1) / 2 + 1;
    topInds  = startInd:(startInd + iCol - 2);
    ii       = (iCol+1):nNodes;
    botInds  = (ii - 2) .* (ii - 1) ./ 2 + iCol;
    edgeInds = [topInds, botInds];
end%if
end%edge_inds
% [EOF]