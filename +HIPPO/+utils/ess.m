function ESS = ess(gamma)
%ESS  effective sample size
%
% ESS = ess(SAMPLES) calculates the effective sample size for a vector
%   parameter, samples of which are held in the columns of SAMPLES. 


%	References:
%	
%	

%	Copyright 2015 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 25-Apr-2015 21:42:08

nSamples = ROInets.cols(gamma);

% compute mean and variance
mu       = mean(gamma, 2);
muTmu    = dot(mu, mu);
variance = mean(sum(gamma .* gamma, 1)) - muTmu;

% compute autocorrelation function
sum_rho_k = 0;
for k = 1:nSamples-1,
    rho_k = (mean(sum(gamma(:,1:end-k) .* gamma(:,k+1:end), 1)) - muTmu) / variance;
    sum_rho_k = sum_rho_k + rho_k;
    if rho_k < 0.05,
        break;
    end%if
end%for

% compute ESS
ESS = nSamples / (1 + 2 * sum_rho_k);
end%ess
% [EOF]