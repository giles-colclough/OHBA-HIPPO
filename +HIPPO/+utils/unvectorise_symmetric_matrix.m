function C = unvectorise_symmetric_matrix(V, useDiagonal, preserveNorm)
%UNVECTORISE_SYMMETRIC_MATRIX  returns feature vector to symmetric matrix
%
% C = UNVECTORISE_SYMMETRIC_MATRIX(V) converts feature vector V into a 
%   symmetric matrix C, preserving norms.
%
% C = UNVECTORISE_SYMMETRIC_MATRIX(V, USEDIAGONAL) sets a logical switch
%   USEDIAGONAL [true] which determines if the diagonal of the matrix is to
%   be kept in the feature vector
%
% C = UNVECTORISE_SYMMETRIC_MATRIX(V, USEDIAGONAL, PRESERVENORM) sets a logical
%   switch NOSCALING [true] which determines if the re-scaling of
%   off-diagonal elements to preserve norms is to be performed. 
%
% See also: vectorise_symmetric_matrix

%	References:
%	Barachant et al., "Classification of covariance matrices using a
%	Riemannian-based kernel for BCI applications," http://hal.archives-ouvertes.fr/docs/00/82/04/75/PDF/BARACHANT_Neurocomputing_ForHal.pdf
%	Neurocomputing 112 (2013) 172-178. 

%	Copyright 2014 Giles Colclough
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@eng.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 23-Sep-2014 11:38:42

if nargin < 2, useDiagonal  = true; end
if nargin < 3, preserveNorm = true; end

if size(V,2)>1,
    for m = size(V,2):-1:1,
        C(:,:,m) = HIPPO.utils.unvectorise_symmetric_matrix(V(:,m), useDiagonal, preserveNorm);
    end%for
    return
end%if

validateattributes(V, ...
                   {'numeric', 'logical'}, ...
                   {'vector','nonempty'}, ...
                   mfilename, ...
                   'C', ...
                   1);
               
validateattributes(useDiagonal, {'logical','char'}, {}, mfilename, 'useDiagonal', 2);
validateattributes(preserveNorm, {'logical'}, {'scalar'}, mfilename, 'noScaling', 3);

% find matrix size from vector dimension
if ischar(useDiagonal) && strcmp(useDiagonal, 'scalar');
    % non-unity but constant diagonal element extracted and put first. 
    nrows = (sqrt(8*(length(V)-1) + 1) + 1) ./ 2;
elseif islogical(useDiagonal),
    if useDiagonal,
        nrows = (sqrt(8*length(V) + 1) - 1) ./ 2;
    else
        nrows = (sqrt(8*length(V) + 1) + 1) ./ 2;
    end%if
else
    error('useDiagonal argument should be logical or the character string ''scalar''. \n');
end%if
if preserveNorm, 
    offDiagScale = sqrt(2);
else
    offDiagScale = 1;
end%if

C = zeros(nrows);
triUpInd = triu(true(nrows),1);
diagInd  = logical(eye(nrows));

if ischar(useDiagonal) && strcmp(useDiagonal, 'scalar');
    % non-unity but constant diagonal element extracted and put first. 
    C(triUpInd) = V(2:end) ./ offDiagScale;
    C           = C + C.';
    C(diagInd)  = V(1);
elseif islogical(useDiagonal),
    if useDiagonal, 
        C(triUpInd | diagInd) = V(:);
        C(triUpInd)           = C(triUpInd) ./ offDiagScale;
        C                     = C + triu(C, 1).';
    else
        C(triUpInd) = V(:) ./ offDiagScale;
        C           = C + C.';
        C(diagInd)  = NaN;
    end%if
else
    error('useDiagonal argument should be logical or the character string ''scalar''. \n');
end%if

end%unvectorise_symmetric_matrix