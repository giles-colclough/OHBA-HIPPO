function V = vectorise_symmetric_matrix(C, useDiagonal, preserveNorm)
%VECTORISE_SYMMETRIC_MATRIX  converts symmetric matrix to feature vector
%
% V = VECTORISE_SYMMETRIC_MATRIX(C) converts symmetric matrix C into a
%   feature vector V, preserving norms.
% 
% V = VECTORISE_SYMMETRIC_MATRIX(C, USEDIAGONAL) sets a logical switch
%   USEDIAGONAL [true] which determines if the diagonal of the matrix is to
%   be kept in the feature vector
%
% V = VECTORISE_SYMMETRIC_MATRIX(C, USEDIAGONAL, PRESERVENORM) sets a logical
%   switch PRESERVENORM [true] which determines if the re-scaling of
%   off-diagonal elements to preserve norms is to be performed. 
%
% See also: unvectorise_symmetric_matrix


%	References:
%	Barachant et al., "Classification of covariance matrices using a
%	Riemannian-based kernel for BCI applications," http://hal.archives-ouvertes.fr/docs/00/82/04/75/PDF/BARACHANT_Neurocomputing_ForHal.pdf
%	Neurocomputing 112 (2013) 172-178. 

%	Copyright 2014 Giles Colclough
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 773 $
%	$LastChangedDate: 2015-11-01 18:20:25 +0000 (Sun, 01 Nov 2015) $
%	Contact: giles.colclough@eng.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 23-Sep-2014 11:16:29

if nargin < 2, useDiagonal  = true; end
if nargin < 3, preserveNorm = true; end

if size(C,3)>1,
    for m = size(C,3):-1:1,
        V(:,m) = HIPPO.utils.vectorise_symmetric_matrix(C(:,:,m), useDiagonal, preserveNorm);
    end%for
    return
end%if

validateattributes(C, ...
                   {'numeric', 'logical'}, ...
                   {'2d', 'nonempty', 'nonsparse'}, ...%'finite', 
                   mfilename, ...
                   'C',       ...
                   1);
               
% validateattributes(useDiagonal,  {'logical'}, {'scalar'}, mfilename, 'useDiagonal',  2);
validateattributes(preserveNorm, {'logical'}, {'scalar'}, mfilename, 'preserveNorm', 3);

nrows        = ROInets.rows(C);
diagInds     = logical(eye(nrows));
upperTriInds = triu(true(nrows), 1);
fullTriInds  = (diagInds | upperTriInds);

if preserveNorm, 
    offDiagScale = sqrt(2);
else
    offDiagScale = 1;
end%if

if ischar(useDiagonal) && strcmp(useDiagonal, 'scalar');
    % non-unity but constant diagonal element extracted and put first. 
    V = [median(C(diagInds)); C(upperTriInds) .* offDiagScale];
elseif islogical(useDiagonal),
    if useDiagonal,
        tmp = C;
        tmp(upperTriInds) = tmp(upperTriInds) .* offDiagScale;
        V = tmp(fullTriInds);
    else
        V = C(upperTriInds) .* offDiagScale;
    end%if
else
    error('useDiagonal argument should be logical or the character string ''scalar''. \n');
end%if

end%vectorise_symmetric_matrix