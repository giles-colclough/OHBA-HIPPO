function [partialCorrelation, correlation, meanPCorr, meanCorr] = normalise_samples(Omega_s, Sigma_s)
%NORMALISE_SAMPLES converts samples of covariance matrices to correlations
% 
% [partialCorrelation, correlation] = normalise_samples(OMEGA_S, SIGMA_S)
%  converts samples of subject precision matrices OMEGA_S and covariance
%  matrices SIGMA_S (both nModes x nModes x nSubjects x nSamples) into
%  samples of partial correlation matrices and correlation matrices. 
% 
% [partialCorrelation, correlation, meanPCorr, meanCorr] = normalise_samples(...)
%  adds in the group mean of each.

nSubjects = size(Omega_s, 2);
nSamples  = size(Omega_s, 3);

for iSubject = nSubjects:-1:1,
	for iSample = nSamples:-1:1,
		tmp(:,:,iSample)  = ROInets.convert_precision_to_pcorr(HIPPO.utils.unvectorise_symmetric_matrix(Omega_s(:,iSubject,iSample), true, false));
		tmp2(:,:,iSample) = corrcov(HIPPO.utils.unvectorise_symmetric_matrix(Sigma_s(:,iSubject,iSample), true, false), 1);
	end%for
	partialCorrelation(:,:,iSubject,:) = tmp;
	correlation(:,:,iSubject,:)        = tmp2;
end%for
meanPCorr = squeeze(nanmean(partialCorrelation,3));
meanCorr  = squeeze(nanmean(correlation,3));