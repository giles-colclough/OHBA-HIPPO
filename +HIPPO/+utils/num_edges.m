function n = num_edges(nNodes)
%NUM_EDGES number of edges in a network matrix with nNodes nodes
%
% N = NUM_EDGES(NNODES) returns the number of edges (upper off-diagonal
%   elements) in a square symmetric network matrix of size NNODES. 
n = nNodes * (nNodes - 1) / 2;
end%num_edges