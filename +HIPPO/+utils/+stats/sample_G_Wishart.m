function K = sample_G_Wishart(S, m, G, matchParameterisation, C)
%SAMPLE_G_WISHART  samples from constrained Wishart distribution
%
% K = SAMPLE_G_WISHART(Sigma, m, G, match, C) samples from a Wishart
%   distribution with scale covariance matrix Sigma, m degrees of freedom
%   and graph G. If match is true, the parameters match the standard
%   parameterisation of the Wishart distribution. If false, the Lenkoski
%   form is used. Can optionally also pass in C, the cholesky decomposition
%   of Sigma.	
%
%   The standard Wishart parameterisation has expectation m*S. The Lenkoski
%   form has expectation (m - p + 1) * inv(S) for a full graph.

%	Copyright 2015 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 856 $
%	$LastChangedDate: 2016-07-17 00:06:20 +0100 (Sun, 17 Jul 2016) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 24-Mar-2015 12:22:49


%% Input checking
[p, check_p] = size(S);
assert(check_p == p,                  ...
       [mfilename ':NotSquareInput'], ...
       'Input scale matrix must be square positive definite. \n');

assert(islogical(G),                      ...
       [mfilename ':EdgeAllocationFail'], ...
       'Edge inclusion matrix must be logical. \n');

if nargin < 4 || ~exist('matchParameterisation', 'var'),
    matchParameterisation = false;
end%if
  
% calculate Cholesky factor of scale matrix
if nargin < 5 || isempty(C) || ~exist('C', 'var'),
    [C, fail] = chol(S);
    if fail,
        error([mfilename ':NotPosDef'], ...
              'Input scale matrix must be positive definite. \n');
    end%if
end%if

% restore parameterisation of standard Wishart
if matchParameterisation,
    df = m; % passed in correctly
else
    % set df for use in samplers
    df = m + p - 1;
end%if
   
%% 1. Sample Sigma^{-1} ~ Wi(S^{-1}, df) 
% - see Lenkoski2013, not error in Hinne 2014, and gwishrnd3 code 
% without optimisation
% Wi(S^{-1}, df) == Wi(D, delta).


% sample U'*U from a Wishart with unit scale
% See http://en.wikipedia.org/wiki/Wishart_distribution#Theorem
U = randwishart(df, p); % Upper chol of unit scale Wishart

% check for complete graph
if all(G(:)),
    if matchParameterisation,
        K = C' * (U' * U) * C;
    else
        % find upper chol of scale matrix in standard param of Wishart
%         Cinv = solve_triu(C, eye(p)); % inv(C); 
%         K    = Cinv * (U' * U) * Cinv';
        Kpart = C \ U';
        K     = Kpart * Kpart';
    end%if
    return
end%if

% reduce operations later by setting diagonal of G to be zero
G0 = logical(min(G, ~eye(p)));

% draw Kstar from full graph
Cinv = solve_triu(C, eye(p));
% could form Kstar as below
% if matchParameterisation,
%     Kstar = C' * (U' * U) * C;
% else
%     Kstar = Cinv * (U' * U) * Cinv';
% end%if
%

% form Sigma = inv(Kstar) directly
Uinv = solve_triu(U, eye(p));

if matchParameterisation,
    Sigma = Cinv * (Uinv * Uinv') * Cinv';
else
    Sigma = C' * (Uinv * Uinv') * C;
end%if

%% 2. Let W = Sigma
W = Sigma;

%% 3. Repeat loop over variables, until convergence
tol = 1e-5;
gap = Inf;
IS_CONVERGED = @(error) abs(error) < tol;
mat_el_prod  = @(M) M(:)' * M(:);
mat_trace    = @(X,Y) X(:)' * Y(:);
not_J        = ~eye(p);

while ~IS_CONVERGED(gap),
%     fprintf('Gap is: %f. \n', gap);
    oldW = W;
    for j = 1:p,
        not_j            = not_J(:,j);
        N_j              = G0(:,j); % use fact that diagonal is 0
        R                = chol(W(N_j,N_j));
        betaStar         = R \ (R' \ Sigma(N_j,j)); % not this: % inverse(W(N_j,N_j),'chol')*Sigma(N_j,j); %
        beta             = zeros(p-1,1);
        beta(N_j(not_j)) = betaStar;
        Wbeta            = W(not_j,not_j) * beta;
        W(not_j,j)       = Wbeta;
        W(j,not_j)       = Wbeta;
    end%loop over variables
    
    gap = mat_trace(W,oldW)./mat_el_prod(oldW) - 1;
end%while

%% 4. Return inverse(W)
K = HIPPO.utils.cholinv(W) .* max(G0, eye(p)); 
end%sample_G_Wishart
% [EOF]