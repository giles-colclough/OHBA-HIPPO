function x = randinvgamma(shape, scale)
%RANDGAMMA random sample from gamma distribution with shape and scale 
%
% X = RANDINVGAMMA(A, B) returns a matrix, the same size as A and B, where
%   X(i,j) is a sample from an Inv-Gamma(A(i,j), B(i,j)) distribution.
%
%   A and B must be the same size. 
%
%   1/X ~ GAMMA(A,1/B)
%
%   Mean:     b/(a-1)
%   Variance: b^2 / ((a-1)^2 (a-2))
%   Skewness: 4 sqrt(a-2) / (a-3)
%   Mode:     b/(a+1)
%
%   Tom Minka's lightspeed toolbox must be installed, compiled, and on the
%   Matlab path. 
%
%   
%   See also gamrnd, randg, randgamma, GC_randgamma. 

%	References:
%	http://en.wikipedia.org/wiki/Inverse-gamma_distribution


%	Copyright 2014 Giles Colclough
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@eng.ox.ac.uk
%	Originally written on: GLNXA64 by Giles Colclough, 19-Feb-2014 16:42:31

% x = GC_randgamma(shape, scale.^(-1)).^(-1); % this code can be used instead but is a little slower
x = scale ./ randgamma(shape);