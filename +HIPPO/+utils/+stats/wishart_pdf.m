function [logp, p] = wishart_pdf(X, V, n, V_R, X_R)
%WISHART_PDF  probability density of Wishart distribution
%
% LOG_P = WISHART_PDF(X, V, N) returns the log probability that X is drawn from a
%   Wishart distribution with scale matrix V and shape n.
%
% [LOGP, P] = WISHART_PDF(...) also returns the probability, p. 
%
% [] = WISHART_PDF(X, V, N, C, CX) passes in the right Cholesky factor of V. 
% [] = WISHART_PDF(X, V, N, C, CX, X_R) passes in the right cholesky factor
%    of X. 
%	
%   https://en.wikipedia.org/wiki/Wishart_distribution
%	

%	Copyright 2015 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 16-Aug-2015 15:29:31

assert(ROInets.rows(X) == ROInets.cols(X) && ROInets.cols(X) == ROInets.cols(V) && ROInets.rows(X) == ROInets.rows(V));

q   = ROInets.rows(X);

if nargin < 4 || ~exist('V_R', 'var') || isempty(V_R),
    V_R = chol(V); 
end%if 
if nargin < 5 || ~exist('X_R', 'var') || isempty(X_R),
    X_R = chol(X);
end%if

logp = (n - q - 1) * 0.5 * logdet(X_R)                     ...
       - 0.5 * trace(solve_triu(V_R, solve_tril(V_R', X))) ... 
       - n * q * 0.5 * log(2)                              ...
       - 0.5 * n * logdet(V_R)                             ...
       - mv_gamma_ln(0.5 * n, q);
   
if nargout > 2,
    p = exp(logp);
end%if
end%wishart_pdf

function v = logdet(A_R)
% log determinant of A, given right cholesky factor A_R. 
v = 2 * sum(log(diag(A_R)));
end%logdet
% [EOF]