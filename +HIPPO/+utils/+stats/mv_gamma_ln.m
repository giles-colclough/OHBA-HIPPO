function G = mv_gamma_ln(a,p)
% MV_GAMMA_LN log-multivariate gamma function
%
% G = MV_GAMMA_LN(a,p) returns the log of the multivariate gamma function,
%   base p, of a: G = log(gamma_p(a)).
%
% See also: GAMMALN, GAMMA. 


% References:
%   https://en.wikipedia.org/wiki/Multivariate_gamma_function


%	Copyright 2015 OHBA
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@magd.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 20-Jul-2015

if 1 == p,
    G = gammaln(a);
    return
end%if

assert(p>1 && ~mod(p,1),                                          ...
       [mfilename ':IncorrectBase'],                              ...
       'P must be an integer greater than 1. Proposed P: %f. \n', ...
       p);
   
for i = length(a):-1:1,
jj    = 1:p;
Gfill = gammaln(a(i) + (1-jj)./2);
G(i)  = 0.25 * p * (p-1) * log(pi) + sum(Gfill);
end%for

G(isnan(G)) = inf;