function J = inverse_permutation(P)
%INVERSE_PERMUTATION	inverts a permutation vector
% J = inverse_permutation(P) produces inverse of permutation P. 
%
% Example:
%   N = 6;
%   A = (1:N)';
%   P = randperm(N)';
%   B = A(P);
%   J = GC_inverse_permutation(P);
%   display(B(J));
%	
%	See also RANDPERM, PERMS. 


%	Copyright 2014 Giles Colclough
%	This program is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%	
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%	
%	You should have received a copy of the GNU General Public License
%	along with this program.  If not, see <http://www.gnu.org/licenses/>.


%	$LastChangedBy: GilesColclough $
%	$Revision: 763 $
%	$LastChangedDate: 2015-10-21 11:52:19 +0100 (Wed, 21 Oct 2015) $
%	Contact: giles.colclough@eng.ox.ac.uk
%	Originally written on: MACI64 by Giles Colclough, 21-Jan-2014 16:22:40

if rows(P) > 1 && cols(P) > 1,
    % the inverse of a permutation matrix is its transpose
    J = P';
else
    % a permutation vector can be reversed like this
    J(P) = 1:length(P);
end%if

end%inverse_permutation