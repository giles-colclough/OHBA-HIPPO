function Prior = default_setup(useExplicitSparcity)
%DEFAULT SETUP for the HIPPO Prior
% 
% Prior = DEFAULT_SETUP()
%  returns a default prior structure good for most circumstances.
%  It will run a Hierarchical version of HIPPO, without a spike-and-slab
%  prior imposing shared sparse structures. 
% 
% Prior = DEFAULT_SETUP(SPARSE) will use the spike and slab prior 
%  if SPARSE is TRUE. The model will need longer to converge. 

% default behaviour is to use the weakly-sparse, non spike-slab system
if ~nargin || isempty(useExplicitSparcity),
	useExplicitSparcity = false;
end%if

Prior = struct();

% Cauchy prior on size of mean effect
Prior.Edges.A       = 0.7;

% Very broad log-space hyperprior on the exponential distribution over diagonal elements
Prior.Lambda.shape  = 1./3; 
Prior.Lambda.rate   = 1e-6; 

% Beta prior on sparsity
if useExplicitSparcity,
	Prior.Beta.a_pi     = 6; 
	Prior.Beta.b_pi     = 6; 
else
	Prior.Beta.a = 1;
end%if

% Control the sparse spike and slab sampler
if useExplicitSparcity,
	Prior.Z.do                  = true;
	Prior.Z.DelayedRejection.do = true;
	Prior.Z.initialGuess        = [];
else
	Prior.Z.do = false;
end

% Rao-Blacwellization is not yet a working option
Prior.RB.do = false;