Hierarchical Inference of Posterior Precisions in OSL

HIPPO





A Matlab package for inferring brain connectivity matrices over multiple subjects with sparse priors
or, for that matter, any group of covariance structures drawn from a similar population


Giles Colclough
December 2016




The HIPPO package samples from a hierarchical Bayesian model 
for sparse precision matrices. It is possible to explicitly
share the sparsity structure between subjects. Information will
be partially pooled across subjects to improve the estimation 
of correlation and partial correlation matrices on each individual. 

A full description of the model can be found in chapter 5 of 
Colclough, G. L. 'Methods for modelling human functional brain networks with MEG and fMRI.' 
DPhil thesis, University of Oxford, Oxford, UK, 2016.


This is a Matlab package. The +HIPPO folder should be visible on your path 
— But the folder contents need not be on your path.
This is the most common error in running the code.


For help running the code, see
HIPPO.sampler.sample
and 
HIPPO.demo.run_demo


Dependencies:
- MATLAB (not tested in Octave)
- MATLAB stats package (it will run, with a few minor modifications, with FieldTrip’s replacement stats package)
- Tom Minka’s LightSpeed toolbox, for fast compiled sampling functions
- OHBA’s ROInets package




